
### 一、签到签退
#### 1.1 签到
地址：/api/sign/signIn

参数：
```json
{
    "apiId":{}
}
```
响应：
```json
```

#### 1.2 签退
地址：/api/sign/signIn

参数：
```json
{
    "apiId":{}
}
```
响应：
```json
```

### 二、人员信息
#### 2.1 划卡人员信息获取
地址：/api/person/get

参数：
```json
{
    "apiId":{},
    "mdtrtCertType":{就诊凭证类型:为“01”时填写电子凭证令牌，为“02”时填写身份证号，为“03”时填写社会保障卡卡号,就诊凭证类型为“03”时必填},
    "mdtrtCertNo":{就诊凭证编号},
    "cardSn":{卡识别码},
    "psnCertType":{人员证件类型},
    "certno":{证件号码},
    "psnName":{人员姓名},
}
```
响应：
```json
{
  "data": {
    "baseinfo": {
      "psn_no": {人员编号},
      "psn_cert_type": {人员证件类型},
      "certno": {证件号码},
      "psn_name": {人员姓名},
      "gend": {性别},
      "naty": {民族},
      "brdy": {出生日期},
      "age": {年龄}
    },
    "insuinfo": {
        "balc":{余额},
        "insutype":{险种类型},
        "psn_type":{人员类别},
        "psn_insu_stas":{人员参保状态},
        "psn_insu_date":{个人参保日期},
        "paus_insu_date":{暂停参保日期},
        "cvlserv_flag":{公务员标志},
        "insuplc_admdvs":{参保地医保区划},
        "emp_name":{单位名称},
    },
    "idetinfo": [
      {
        "psn_idet_type":{人员身份类别},
        "psn_type_lv":{人员类别等级},
        "memo":{备注},
        "begntime":{开始时间},
        "endtime":{结束时间},
      }
    ]
  }
}
```

### 三、结算相关
#### 3.1 预结算
地址：/api/setl/settlementBefore

参数：
```json
{
    "apiId":{},
    "masterId":{总单Id},
    "totalAmount":{总金额},
    "mdtrtCertType":{就诊凭证类型},
    "mdtrtCertNo":{就诊凭证编号/就诊凭证类型为 “01”时填写电子凭证令牌， 为 “02”时填写身份证号， 为 “03”时填写社会保障卡卡号},
    "medType":{医疗类别},
    "acctUsedFlag":{个人账户使用标志,0：否，1：是},
    "psnNo":{人员编号},
    "insutype":{险种类型},
    "insuplcAdmdvs":{人员参保地区划},
    "detailList":[
        {
            "goodsId":{本地商品ID},
            "goodsQty":{商品数量},
            "price":{商品单价},
            "orderDtlId":{费用明细流水号}
        }
    ]
}
```
响应：
```json
{
    "data":{
        "setlinfo":{
            "mdtrtId":{就诊ID},
            "psnNo":{人员编号},
            "psnName":{人员姓名},
            "psnCertType":{人员证件类型},
            "certno":{证件号码},
            "gend":{性别},
            "naty":{民族},
            "brdy":{出生日期},
            "age":{年龄},
            "insutype":{险种类型},
            "psnType":{人员类别},
            "cvlservFlag":{公务员标志},
            "setlTime":{结算时间},
            "mdtrtCertType":{就诊凭证类型},
            "medType":{医疗类别},
            "medfeeSumamt":{医疗费总额},
            "fulamtOwnpayAmt":{全自费金额},
            "overlmtSelfpay":{超限价自费费用},
            "preselfpayAmt":{先行自付金额},
            "inscpScpAmt":{符合政策范围金额},
            "actPayDedc":{实际支付起付线},
            "hifpPay":{基本医疗保险统筹基金支出},
            "poolPropSelfpay":{基本医疗保险统筹基金支付比例},
            "cvlservPay":{公务员医疗补助资金支出},
            "hifesPay":{企业补充医疗保险基金支出},
            "hifmiPay":{居民大病保险资金支出},
            "hifobPay":{职工大额医疗费用补助基金支出},
            "mafPay":{医疗救助基金支出},
            "othPay":{其他支出},
            "fundPaySumamt":{基金支付总额},
            "psnPartAmt":{个人负担总金额},
            "acctPay":{个人账户支出},
            "psnCashPay":{个人现金支出},
            "balc":{余额},
            "acctMulaidPay":{个人账户共济支付金额},
            "medinsSetlId":{医药机构结算ID},
            "clrOptins":{清算经办机构},
            "clrWay":{清算方式},
            "clrType":{清算类别},
        },
        "setldetail":{
            "fundPayType":{基金支付类型},
            "inscpScpAmt":{符合政策范围金额},
            "crtPaybLmtAmt":{本次可支付限额金额},
            "fundPayamt":{基金支付金额},
            "fundPayTypeName":{基金支付类型名称},
            "setlProcInfo":{结算过程信息},
        },
        "detlcutinfo":{
            "feedetlSn":{费用明细流水号},
            "detItemFeeSumamt":{明细项目费用总额},
            "cnt":{数量},
            "pric":{单价},
            "pricUplmtAmt":{定价上限金额},
            "selfpayProp":{自付比例},
            "fulamtOwnpayAmt":{全自费金额},
            "overlmtAmt":{超限价金额},
            "preselfpayAmt":{先行自付金额},
            "inscpScpAmt":{符合政策范围金额},
            "chrgitmLv":{收费项目等级},
            "medChrgitmType":{医疗收费项目类别},
            "basMednFlag":{基本药物标志},
            "hiNegoDrugFlag":{医保谈判药品标志},
            "chldMedcFlag":{儿童用药标志},
            "listSpItemFlag":{目录特项标志},
            "drtReimFlag":{直报标志},
            "memo":{备注},
        },
    }
}
```

#### 3.2 结算
地址：/api/setl/settlement

参数：
```json
{
    "apiId":{},
    "masterId":{总单Id},
    "totalAmount":{总金额},
    "mdtrtCertType":{就诊凭证类型},
    "mdtrtCertNo":{就诊凭证编号/就诊凭证类型为 “01”时填写电子凭证令牌， 为 “02”时填写身份证号， 为 “03”时填写社会保障卡卡号},
    "medType":{医疗类别},
    "acctUsedFlag":{个人账户使用标志,0：否，1：是},
    "psnNo":{人员编号},
    "insutype":{险种类型},
    "insuplcAdmdvs":{人员参保地区划},
    "msgId":{发送方报文ID},
    "detailList":[
        {
            "goodsId":{本地商品ID},
            "goodsQty":{商品数量},
            "price":{商品单价},
            "orderDtlId":{费用明细流水号}
        }
    ]
}
```
响应：
```json
{
    "data":{
        "msgId":{接收方报文ID},
        "psnNo":{人员编号},
        "setlId":{医保结算ID},
        "balc":{余额},
        "setlinfo":{
            "setlId":{结算ID},
            "mdtrtId":{就诊ID},
            "psnNo":{人员编号},
            "psnName":{人员姓名},
            "psnCertType":{人员证件类型},
            "certno":{证件号码},
            "gend":{性别},
            "naty":{民族},
            "brdy":{出生日期},
            "age":{年龄},
            "insutype":{险种类型},
            "psnType":{人员类别},
            "cvlservFlag":{公务员标志},
            "setlTime":{结算时间},
            "mdtrtCertType":{就诊凭证类型},
            "medType":{医疗类别},
            "medfeeSumamt":{医疗费总额},
            "fulamtOwnpayAmt":{全自费金额},
            "overlmtSelfpay":{超限价自费费用},
            "preselfpayAmt":{先行自付金额},
            "inscpScpAmt":{符合政策范围金额},
            "actPayDedc":{实际支付起付线},
            "hifpPay":{基本医疗保险统筹基金支出},
            "poolPropSelfpay":{基本医疗保险统筹基金支付比例},
            "cvlservPay":{公务员医疗补助资金支出},
            "hifesPay":{企业补充医疗保险基金支出},
            "hifmiPay":{居民大病保险资金支出},
            "hifobPay":{职工大额医疗费用补助基金支出},
            "mafPay":{医疗救助基金支出},
            "othPay":{其他支出},
            "fundPaySumamt":{基金支付总额},
            "psnPartAmt":{个人负担总金额},
            "acctPay":{个人账户支出},
            "psnCashPay":{个人现金支出},
            "balc":{余额},
            "acctMulaidPay":{个人账户共济支付金额},
            "medinsSetlId":{医药机构结算ID},
            "clrOptins":{清算经办机构},
            "clrWay":{清算方式},
            "clrType":{清算类别},
        },
        "setldetail":{
            "fundPayType":{基金支付类型},
            "inscpScpAmt":{符合政策范围金额},
            "crtPaybLmtAmt":{本次可支付限额金额},
            "fundPayamt":{基金支付金额},
            "fundPayTypeName":{基金支付类型名称},
            "setlProcInfo":{结算过程信息},
        },
        "detlcutinfo":{
            "feedetlSn":{费用明细流水号},
            "detItemFeeSumamt":{明细项目费用总额},
            "cnt":{数量},
            "pric":{单价},
            "pricUplmtAmt":{定价上限金额},
            "selfpayProp":{自付比例},
            "fulamtOwnpayAmt":{全自费金额},
            "overlmtAmt":{超限价金额},
            "preselfpayAmt":{先行自付金额},
            "inscpScpAmt":{符合政策范围金额},
            "chrgitmLv":{收费项目等级},
            "medChrgitmType":{医疗收费项目类别},
            "basMednFlag":{基本药物标志},
            "hiNegoDrugFlag":{医保谈判药品标志},
            "chldMedcFlag":{儿童用药标志},
            "listSpItemFlag":{目录特项标志},
            "drtReimFlag":{直报标志},
            "memo":{备注},
        }
    }
}
```

#### 3.3 结算撤销
地址：/api/setl/settlementReturn

参数：
```json
{
    "apiId":{},
    "psnNo":{人员编号},
    "setlId":{结算ID},
    "mdtrtId":{就诊ID},
    "insuplcAdmdvs":{人员参保地医保区划}
}
```
响应：
```json
{
    "data":{
        "setlinfo": {
            "mdtrtId":{就诊ID},
            "setlId":{结算ID},
            "clrOptins":{清算经办机构},
            "medfeeSumamt":{医疗费总额},
            "setlTime":{结算时间},
            "fulamtOwnpayAmt":{全自费金额},
            "overlmtSelfpay":{超限价自费费用},
            "preselfpayAmt":{先行自付金额},
            "inscpScpAmt":{符合政策范围金额},
            "actPayDedc":{实际支付起付线},
            "hifpPay":{基本医疗保险统筹基金支出},
            "poolPropSelfpay":{基本医疗保险统筹基金支付比例},
            "cvlservPay":{公务员医疗补助资金支出},
            "hifesPay":{企业补充医疗保险基金支出},
            "hifmiPay":{居民大病保险资金支出},
            "hifobPay":{职工大额医疗费用补助基金支出},
            "mafPay":{医疗救助基金支出},
            "othPay":{其他支出},
            "fundPaySumamt":{基金支付总额},
            "psnPay":{个人支付金额},
            "acctPay":{个人账户支出},
            "cashPayamt":{现金支付金额},
            "balc":{余额},
            "acctMulaidPay":{个人账户共济支付金额},
            "medinsSetlId":{医药机构结算ID}
        },
        "setldetail":{
            "fundPayType":{基金支付类型},
            "inscpScpAmt":{符合政策范围金额},
            "crtPaybLmtAmt":{本次可支付限额金额},
            "fundPayamt":{基金支付金额},
            "fundPayTypeName":{基金支付类型名称},
            "setlProcInfo":{结算过程信息}
        }
    }
}
```

#### 3.4 冲正
地址：/api/setl/correct

参数：
```json
{
    "apiId":{},
    "psnNo":{人员编号},
    "omsgid":{原发送方报文ID},
    "oinfno":{原交易编号},
    "insuplcAdmdvs":{人员参保地医保区划}
}
```
响应：
```json
```

#### 3.5 查询商品医保对照结果
请求地址：/open/queryMedicareRel

请求参数：
```json
{
    "apiId":{},
    "goodsIdList":[
        {本地标识ID}
    ]
}
```
响应参数：
```json
{
    "code": 0,
    "message": "操作成功",
    "data": {
        "297922": {
            "medicareDrugId": {医保目录ID},
            "drugCode": {医保目录编码},
            "medicareCatalogId": {目录ID},
            "categroyType": {目录类型},
            "approved": {审批状态/0 未审批,1 已通过,2 未通过}
        }
    }
}
```



### 四、进销存管理
#### 4.1 商品盘存上传
地址：/api/goods/goodsTakeStock

参数：
```json
{
    "apiId":{},
    "goodsId":{本地商品ID},
    "goodsQty":{商品数量},
    "lotNum":{批号},
    "lotNumId":{批号ID},
    "invdate":{盘存时间},
    "manuDate":{生产日期},
    "expyEnd":{有效期止}
}
```
响应：
```json
```

#### 4.2 商品库存变更
地址：/api/goods/goodsStockChange

参数：
```json
{
    "apiId":{},
    "goodsId":{本地商品ID},
    "invChgType":{库存变更类型、101	调拨入库，105销毁，102调拨出库，106其他入库，103盘盈，107其他出库，104盘损，108初始化入库},
    "lotNumId":{批号ID},
    "price":{单价},
    "goodsQty":{商品数量},
    "invChgTime":{库存变更时间}
}
```
响应：
```json
```

#### 4.3 商品采购
地址：/api/goods/goodsPurchase

参数：
```json
{
    "apiId":{},
    "goodsId":{本地商品ID},
    "dyntNo":{随货单号},
    "lotNumId":{批号ID},
    "lotNum":{批号},
    "splerName":{供应商名称},
    "splerPmtNo":{供应商许可证号},
    "prodentpName":{厂家名字},
    "approvalNo":{批准文号},
    "manuDate":{生产日期 yyyy-MM-dd},
    "expyEnd":{有效期止 yyyy-MM-dd},
    "finlTrnsPric":{最终成交价},
    "purcRetnCnt":{采购/退货 数量},
    "purcRetnStoinTime":{采购/退货入库 时间},
    "purcRetnOpterName":{采购/退货经办人姓名}
}
```
响应：
```json
```

#### 4.4 商品采购退货
地址：/api/goods/goodsPurchaseReturn

参数：
```json
{
    "apiId":{},
    "goodsId":{本地商品ID},
    "lotNumId":{批号ID},
    "splerName":{供应商名称},
    "splerPmtNo":{供应商许可证号},
    "manuDate":{生产日期 yyyy-MM-dd},
    "expyEnd":{有效期止 yyyy-MM-dd},
    "finlTrnsPric":{最终成交价},
    "purcRetnCnt":{采购/退货 数量},
    "purcRetnStoinTime":{采购/退货入库时间},
    "purcRetnOpterName":{采购/退货经办人姓名},
    "purcInvoNo":{采购发票号}
}
```
响应：
```json
```

#### 4.5 商品销售
地址：/api/goods/goodsSale

参数：
```json
{
    "apiId":{},
    "setlId":{结算ID},
    "selRetnOpterName":{销售/退货经办人姓名},
    "goodsList":[
        {
            "goodsId":{本地商品ID},
            "lotNumId":{批号ID},
            "lotNum":{批号},
            "manuDate":{生产日期 yyyy-MM-dd},
            "expyEnd":{有效期止 yyyy-MM-dd},
            "finlTrnsPric":{最终成交单价},
            "prscDrName":{开方医师姓名},
            "pharName":{药师姓名},
            "pharPracCertNo":{药师执业资格证号},
            "trdnFlag":{拆零标志/0-否；1-是},
            "rtalDocno":{零售单据号},
            "selRetnCnt":{销售/退货数量},
            "selRetnTime":{销售/退货时间},
        }
    ]
}
```
响应：
```json
```

#### 4.6 商品销售退回
地址：/api/goods/goodsSaleReturn

参数：
```json
{
    "apiId":{},
    "setlId":{结算ID},
    "selRetnOpterName":{销售/退货经办人姓名},
    "goodsList":[
        {
            "goodsId":{本地商品ID},
            "lotNumId":{批号ID},
            "lotNum":{批号},
            "manuDate":{生产日期 yyyy-MM-dd},
            "expyEnd":{有效期止 yyyy-MM-dd},
            "finlTrnsPric":{最终成交单价},
            "trdnFlag":{拆零标志},
            "selRetnCnt":{销售/退货数量},
            "selRetnTime":{销售/退货时间},
        }
    ]
}
```
响应：
```json
```

