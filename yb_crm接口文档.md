## 医保接口文档

#### 1.用户信息
##### 1.1 用户注册
请求地址：/web/admin/save

请求参数：
```json
{
    "loginName":{登录名},
    "loginPwd":{密码md5加密},
    "fullName":{全名},
    "shopName":{门店名称},
    "mobile":{手机号}
}
```
响应参数：
```json
{}
```

##### 1.2 用户登录
请求地址：/web/admin/login

请求参数：
```json
{
    "loginName":{登录名},
    "loginPwd":{密码md5加密},
    "captchaNo":{验证码ID},
    "code":{验证码}
}
```
响应参数：
```json
{
    "code": 0,
    "message": "操作成功",
    "data": {
        "adminInfo": {
            "adminId": {用户ID},
            "loginName": {登录名称},
            "registerTime": {注册时间},
            "status": 0,
            "dataCreated": {录入日期},
            "lastUpdated": {修改日期}
        },
        "token": {token令牌}
    }
}
```

##### 1.4 查询用户api信息
请求地址：/web/apiUser/get

请求参数：
```json
{}
```
响应参数：
```json
{
    "code": 0,
    "message": "操作成功",
    "data": {
        "apiUserId": {apiUserId},
        "apiId": {apiId},
        "apiSecret": {apiSecret},
        "status": {是否启用/0: 启用,1：停用},
        "expiryDate": {过期日期}
    }
}
```

##### 1.5 根据apiUserId查询机器信息
请求地址：/web/apiMachine/get

请求参数：
```json
{
    "apiUserId":{apiUserId}
}
```
响应参数：
```json
{
    "code": 0,
    "message": "操作成功",
    "data": [
        {
            "apiMachineId": {机器信息ID},
            "apiUserId": {apiUserId},
            "machinedNo": {机器码},
            "status": {状态},
            "dataCreated": {录入日期},
            "lastUpdated": {修改日期}
        }
    ]
}
```

#####  1.6 修改用户信息
请求地址：/web/admin/update

请求参数：
```json
{
    "fullName":{全名},
    "shopName":{门店名称},
    "mobile":{手机号}
}
```
响应参数：
```json
{
    {标准响应}
}
```


##### 1.7 数据中心查询用户API信息
请求地址：/sys/sysApiUser/query

请求参数：
```json
{
    "page": {
        "currentNum": 0,
        "pageSize": 20
    },
    "apiId":{apiId},
    "companyId":{公司ID},
    "expiryDateStart":{过期时间开始},
    "expiryDateEnd":{过期时间结束},
    "dataCreatedStart":{创建日期开始},
    "dataCreatedEnd":{创建日期结束}
}
```
响应参数：
```json
{
    "code": 0,
    "message": "操作成功",
    "data": {
        "content": [
            {
                "apiUserId": {apUserId},
                "expiryDate": {过期日期},
                "lastUpdated": {修改日期},
                "companyId": {公司ID},
                "companyName": {公司名称},
                "apiSecret": {apiSecret},
                "dataCreated": {录入日期},
                "apiId": {apiId},
                "status": {是否启用/0: 启用,1：停用},
                "fullName":{人员名称},
                "loginName":{登录名},
                "mobile":{手机号},
                "shopName":{门店名称}
            }
        ],
        "pageable": {
            "sort": {
                "sorted": true,
                "unsorted": false,
                "empty": false
            },
            "offset": 0,
            "pageSize": 20,
            "pageNumber": 0,
            "paged": true,
            "unpaged": false
        },
        "totalElements": 6,
        "totalPages": 1,
        "last": true,
        "number": 0,
        "size": 20,
        "sort": {
            "sorted": true,
            "unsorted": false,
            "empty": false
        },
        "numberOfElements": 6,
        "first": true,
        "empty": false
    }
}
```

##### 1.8 数据中心查询用户
请求地址：/sys/admin/query

请求参数：
```json
{
    "apiUserId":{apiUserId},
    "page": {
        "currentNum": 0,
        "pageSize": 20
    }
}
```
响应参数：
```json
{
    "code": 0,
    "message": "操作成功",
    "data": {
        "content": [
            {
                "adminId": {用户ID},
                "loginName": {登录名称},
                "registerTime": {注册时间},
                "status": {状态/0: 启用,1：停用},
                "apiUserId": {},
                "mobile": {手机号},
                "dataCreated": {录入日期},
                "lastUpdated": {修改日期}
            }
        ],
        "pageable": {
            "sort": {
                "sorted": true,
                "unsorted": false,
                "empty": false
            },
            "offset": 0,
            "pageNumber": 0,
            "pageSize": 20,
            "unpaged": false,
            "paged": true
        },
        "totalElements": 1,
        "last": true,
        "totalPages": 1,
        "number": 0,
        "size": 20,
        "sort": {
            "sorted": true,
            "unsorted": false,
            "empty": false
        },
        "numberOfElements": 1,
        "first": true,
        "empty": false
    }
}
```

##### 1.9 数据中心查询admin信息
请求地址：/sys/admin/queryList

请求参数：
```json
{
    "loginName":{登录名称},
    "page": {
        "currentNum": 0,
        "pageSize": 20
    }
}
```
响应参数：
```json
{
    "code": 0,
    "message": "操作成功",
    "data": {
        "content": [
            {
                "adminId": {用户ID},
                "loginName": {登录名称},
                "registerTime": {注册时间},
                "status": {状态/0: 启用,1：停用},
                "apiUserId": {},
                "mobile": {手机号},
                "dataCreated": {录入日期},
                "lastUpdated": {修改日期}
            }
        ],
        "pageable": {
            "sort": {
                "sorted": true,
                "unsorted": false,
                "empty": false
            },
            "offset": 0,
            "pageNumber": 0,
            "pageSize": 20,
            "unpaged": false,
            "paged": true
        },
        "totalElements": 1,
        "last": true,
        "totalPages": 1,
        "number": 0,
        "size": 20,
        "sort": {
            "sorted": true,
            "unsorted": false,
            "empty": false
        },
        "numberOfElements": 1,
        "first": true,
        "empty": false
    }
}
```


##### 1.10 数据中心根据apiUserId查询机器信息
请求地址：/sys/apiMachine/get

请求参数：
```json
{
    "apiUserId":{apiUserId}
}
```
响应参数：
```json
{
    "code": 0,
    "message": "操作成功",
    "data": [
        {
            "apiMachineId": {机器信息ID},
            "apiUserId": {apiUserId},
            "machinedNo": {机器码},
            "status": {状态},
            "dataCreated": {录入日期},
            "lastUpdated": {修改日期}
        }
    ]
}
```

##### 1.11 数据中心删除指定用户机器信息
请求地址：/sys/apiMachine/delete

请求参数：
```json
{
    "apiMachineId":{机器信息ID}
}
```
响应参数：
```json
{
    {标准响应}
}
```

##### 1.12 数据中心启用用户
请求地址：/sys/admin/enable

请求参数：
```json
{
    "adminId":{用户ID}
}
```
响应参数：
```json
{
    {标准响应}
}
```

##### 1.13 数据中心停用用户
请求地址：/sys/admin/deactivate

请求参数：
```json
{
    "adminId":{用户ID}
}
```
响应参数：
```json
{
    {标准响应}
}
```

##### 1.14 查询用户操作员信息
请求地址：/web/apiOperator/get

请求参数：
```json
{}
```
响应参数：
```json
{
    "code": 0,
    "message": "操作成功",
    "data": {
        "apiOperatorId": {操作员ID},
        "apiUserId": {apiUserId},
        "operatorNo": {操作员编号},
        "dataCreated": {录入日期},
        "lastUpdated": {修改日期}
    }
}
```

##### 1.15 数据中心停用apiUser
请求地址：/sys/sysApiUser/deactivate

请求参数：
```json
{
    "apiUserId":{apiUserId}
}
```
响应参数：
```json
{
    {标准响应}
}
```

##### 1.16 数据中心启用apiUser
请求地址：/sys/sysApiUser/enable

请求参数：
```json
{
    "apiUserId":{apiUserId}
}
```
响应参数：
```json
{
    {标准响应}
}
```

##### 1.17 数据中心修改apiUser过期日期
请求地址：/sys/sysApiUser/update

请求参数：
```json
{
    "apiUserId":{apiUserId},
    "expiryDate":{到期时间}
}
```
响应参数：
```json
{
    {标准响应}
}
```

##### 1.18 用户修改密码
请求地址：/web/admin/updatePwd

请求参数：
```json
{
    "loginPwd":{原密码},
    "resetPwd":{新密码}
}
```
响应参数：
```json
{
    {标准响应}
}
```

##### 1.19 清空用户商品对照关系
请求地址：/sys/sysApiUser/clearDrugGoodsRel

请求参数：
```json
{
    "apiUserId":{apiUserId}
}
```
响应参数：
```json
{
    {标准响应}
}
```


##### 1.20 就诊信息查询接口
请求地址：/sys/admin/queryMedicalInfo

请求参数：
```json
{
    "loginName":{登录名称},
    "psnNo":{人员编号},
    "medType":{医疗类别},
    "begntime":{开始时间yyyy-MM-dd HH:mm:ss},
    "endtime":{结束时间yyyy-MM-dd HH:mm:ss},
    "mdtrtId":{就诊ID/选填}
}
```
响应参数：
```json
{
  "code": 0,
  "message": "操作成功",
  "data": [
    {
      "medType": {医疗类别},
      "cvlservFlag": {公务员标志},
      "empName": {单位名称},
      "opterId": {经办人ID},
      "brdy": {出生日期},
      "naty": {民族},
      "inhospStas": {在院状态},
      "certno": {证件号码},
      "flxempeFlag": {灵活就业标志},
      "psnNo": {人员编号},
      "mdtrtCertType": {就诊凭证类型},
      "opterName": {经办人姓名},
      "psnCertType": {人员证件类型},
      "gend": {性别},
      "mdtrtId": {就诊ID},
      "optTime": {经办时间},
      "begntime": {开始时间},
      "endtime": {结束时间},
      "insutype": {险种类型},
      "prePayFlag": {先行支付标志},
      "psnName": {人员姓名},
      "insuOptins": {参保机构医保区划},
      "oprnOprtName": {手术操作名称},
      "psnType": {人员类别},
      "age": {年龄}
    }
  ]
}
```

##### 1.21 费用明细查询
请求地址：/sys/admin/queryExpenseDetail

请求参数：
```json
{
    "loginName":{登录名称},
    "psnNo":{人员编号},
    "mdtrtId":{就诊ID}
}
```
响应参数：
```json
{
  "code": 0,
  "message": "操作成功",
  "data": [
    {
      "basMednFlag": {基本药物标志},
      "medChrgitmType": {医疗收费项目类别},
      "hiNegoDrugFlag": {医保谈判药品标志},
      "medListCodg": {医疗目录编码},
      "medType": {医疗类别},
      "hilistCode":{医保目录编码},
      "opterId": {经办人ID},
      "medinsListName": {医药机构目录名称},
      "pricUplmtAmt": {定价上限金额},
      "payloc": {支付地点/1中心,2医疗机构,3省内异地,4跨省异地,5互联网医院},
      "opterName": {经办人姓名},
      "selfpayProp": {自付比例},
      "feeOcurTime": {费用发生时间},
      "lmtUsedFlag": {限制使用标志},
      "detItemFeeSumamt": {明细项目费用总额},
      "hilistName": {医保目录名称},
      "mdtrtId": {就诊ID},
      "optTime": {经办时间},
      "fulamtOwnpayAmt":{全自费金额},
      "pric": {单价},
      "cnt": {数量},
      "matnFeeFlag": {生育费用标志0	否	1	是},
      "setlId": {结算ID},
      "feedetlSn": {费用明细流水号},
      "inscpScpAmt": {符合政策范围金额},
      "drtReimFlag": {直报标志},
      "listType": {目录类别},
      "overlmtAmt": {超限价金额},
      "medinsListCodg": {医药机构目录编码},
      "preselfpayAmt": {先行自付金额},
      "chrgitmLv": {收费项目等级}
    }
  ]
}
```
##### 1.22 撤销订单接口
请求地址：/sys/admin/setlReturn

请求参数：
```json
{
    "loginName":{登录名称},
    "psnNo":{人员编号},
    "setlId":{结算ID},
    "mdtrtId":{就诊ID},
    "insuplcAdmdvs":{人员参保地区划},
    "certno":{证件号码},
    "psnName":{人员姓名}
}
```
响应参数：
```json
{
    "data":{
        "setlinfo": {
            "mdtrtId":{就诊ID},
            "setlId":{结算ID},
            "clrOptins":{清算经办机构},
            "medfeeSumamt":{医疗费总额},
            "setlTime":{结算时间},
            "fulamtOwnpayAmt":{全自费金额},
            "overlmtSelfpay":{超限价自费费用},
            "preselfpayAmt":{先行自付金额},
            "inscpScpAmt":{符合政策范围金额},
            "actPayDedc":{实际支付起付线},
            "hifpPay":{基本医疗保险统筹基金支出},
            "poolPropSelfpay":{基本医疗保险统筹基金支付比例},
            "cvlservPay":{公务员医疗补助资金支出},
            "hifesPay":{企业补充医疗保险基金支出},
            "hifmiPay":{居民大病保险资金支出},
            "hifobPay":{职工大额医疗费用补助基金支出},
            "mafPay":{医疗救助基金支出},
            "othPay":{其他支出},
            "fundPaySumamt":{基金支付总额},
            "psnPay":{个人支付金额},
            "acctPay":{个人账户支出},
            "cashPayamt":{现金支付金额},
            "balc":{余额},
            "acctMulaidPay":{个人账户共济支付金额},
            "medinsSetlId":{医药机构结算ID}
        },
        "setldetail":{
            "fundPayType":{基金支付类型},
            "inscpScpAmt":{符合政策范围金额},
            "crtPaybLmtAmt":{本次可支付限额金额},
            "fundPayamt":{基金支付金额},
            "fundPayTypeName":{基金支付类型名称},
            "setlProcInfo":{结算过程信息}
        }
    }
}
```

##### 1.23 查询用户医保商品对照结果
请求地址：/sys/sysMedicareGoodsRel/queryContrastResult

请求参数：
```json
{
    "apiUserId":{apiUserId}
}
```
响应参数：
```json
{
    "code": 0,
    "message": "操作成功",
    "data": {
        "notApproved": {未审批数量},
        "notPassed": {未通过数量},
        "passed": {已审批数量}
    }
}
```


#### 2.医保商品对应本地商品

##### 2.1 医保商品对应本地商品查询
请求地址：/web/medicareDrugsGoodsRel/query

请求参数：
```json
{
    "page": {
        "currentNum": 当前页,
        "pageSize": 每页条数
    },
    "goodsId":{本地商品ID},
    "drugName":{通用中文名称},
    "approvalNo":{国药准字},
    "drugCode":{医保商品编码}
}
```
响应参数：
```json
{
    "code": 0,
    "message": "操作成功",
    "data": {
        "content": [
            {
                "approvalNo": {国药准字},
                "productionUnit": {生产单位},
                "endDate": {终止时间/YYYYMMDDHH24MISS},
                "basicMedicalFlag": {基本医疗使用标志},
                "childMedicationFlag": {儿童用药标志},
                "memo": {备注},
                "outpatientPayRatio": {普通门诊自付比例},
                "medicareDrugsGoodsRelId": {关系表ID},
                "frequency": {使用频次},
                "lastUpdated": {修改日期},
                "handleDate": {经办日期},
                "drugSpecs": {规格},
                "countryCatalogCode": {国家目录编码},
                "minPackQty": {最小包装数量},
                "herbalManageMethod": {中草药管理办法},
                "priceCeiling": {最高限价},
                "drugDosageUnit": {药品剂量单位},
                "factoryName": {药厂名称},
                "limitedDay": {限定天数},
                "nationalDrugCode": {药监局药品编码},
                "prescriptionFlag": {处方药标志},
                "approvedDate": {批准日期},
                "fiveMnemonic": {五笔助记码},
                "unit": {单位},
                "companyId": {公司ID},
                "fertilityFlag": {生育使用标志},
                "pinyin": {拼音码},
                "registeredName": {药品注册名称},
                "drugName": {通用中文名称},
                "needApproval": {是否需要审批标志},
                "startDate": {开始时间},
                "dosage": {用法},
                "englishName": {英文名称},
                "chargeCategory": {收费类别/11--西药，12--中成药，13--中草药，15—蒙药},
                "drugCode": {药监局药品编码},
                "validFlag": {有效标志},
                "goodsId": {本地商品ID},
                "eachDosage": {每次用量},
                "workInjuryFlag": {工伤使用标志},
                "hospitalFlag": {院内制剂标志},
                "customCode": {自定义码},
                "approvalRemark": {批准文号备注},
                "limitScope": {限制使用范围},
                "drugClass": {药品类别码},
                "adminId": {登陆人ID},
                "outpatientFlag": {门诊统筹标志},
                "dataCreated": {创建日期},
                "medicareDrugId": {医保药品目录ID},
                "retirementPayRatio": {离休自付比例},
                "systemId": {系统ID},
                "drugTradeName": {药品商品名},
                "chargeLevel": {收费项目等级},
                "medicine": {剂型},
                "medicalInstitutionNumber": {医疗机构编号},
                "baseDrugFlag": {国家基本药物标志},
                "packMaterial": {包装材质},
                "apiUserId": {api 用户ID},
                "hospitalizedPayRatio": {住院自付比例},
                "majorFunctions": {主要适用症},
                "approved": {是否审批通过/0 未通过,1 已通过},
            }
        ],
        "pageable": {
            "sort": {
                "unsorted": false,
                "sorted": true,
                "empty": false
            },
            "offset": 2,
            "pageSize": 1,
            "pageNumber": 2,
            "paged": true,
            "unpaged": false
        },
        "last": true,
        "totalPages": 3,
        "totalElements": 3,
        "number": 2,
        "size": 1,
        "first": false,
        "sort": {
            "unsorted": false,
            "sorted": true,
            "empty": false
        },
        "numberOfElements": 1,
        "empty": false
    }
}
```

##### 2.2医保商品对应本地商品新增
请求地址：/web/medicareDrugsGoodsRel/save

请求参数：
```json
{
    "medicareDrugId":{医保目录商品ID},
    "goodsId":{本地商品ID},
    "adminId":{用户ID},
    "companyId":{公司ID},
    "systemId":{系统ID}
}
```
响应参数：
```json
{
    {标准响应}
}
```

##### 2.3医保商品对应本地商品修改
请求地址：/web/medicareDrugsGoodsRel/update

请求参数：
```json
{
    "medicareDrugsGoodsRelId":{关系表ID},
    "goodsId":{本地商品ID}
}
```
响应参数：
```json
{
    {标准响应}
}
```
##### 2.4医保商品对应本地商品删除
请求地址：/web/medicareDrugsGoodsRel/delete

请求参数：
```json
{
    "ids":[{对应关系ID}]
}
```
响应参数：
```json
{
    {标准响应}
}
```

##### 2.5 医保商品目录查询
请求地址：/web/medicareDrug/query

请求参数：
```json
{
    "page": {
        "currentNum": 0,
        "pageSize": 20
    },
    "drugName":{药品名称},
    "approvalNo":{国药准字},
    "drugCode":{药品编码}
}
```
响应参数：
```json
{
    "code": 0,
    "message": "操作成功",
    "data": {
        "content": [
            {
                "approvalNo": {国药准字},
                "productionUnit": {生产单位},
                "endDate": {终止时间/YYYYMMDDHH24MISS},
                "basicMedicalFlag": {基本医疗使用标志},
                "childMedicationFlag": {儿童用药标志},
                "memo": {备注},
                "outpatientPayRatio": {普通门诊自付比例},
                "frequency": {使用频次},
                "lastUpdated": {修改日期},
                "handleDate": {经办日期},
                "drugSpecs": {规格},
                "countryCatalogCode": {国家目录编码},
                "minPackQty": {最小包装数量},
                "herbalManageMethod": {中草药管理办法},
                "priceCeiling": {最高限价},
                "drugDosageUnit": {药品剂量单位},
                "factoryName": {药厂名称},
                "limitedDay": {限定天数},
                "nationalDrugCode": {药监局药品编码},
                "prescriptionFlag": {处方药标志},
                "approvedDate": {批准日期},
                "fiveMnemonic": {五笔助记码},
                "unit": {单位},
                "companyId": {公司ID},
                "fertilityFlag": {生育使用标志},
                "pinyin": {拼音码},
                "registeredName": {药品注册名称},
                "drugName": {通用中文名称},
                "needApproval": {是否需要审批标志},
                "startDate": {开始时间},
                "dosage": {用法},
                "englishName": {英文名称},
                "chargeCategory": {收费类别/11--西药，12--中成药，13--中草药，15—蒙药},
                "drugCode": {药监局药品编码},
                "validFlag": {有效标志},
                "eachDosage": {每次用量},
                "workInjuryFlag": {工伤使用标志},
                "hospitalFlag": {院内制剂标志},
                "customCode": {自定义码},
                "approvalRemark": {批准文号备注},
                "limitScope": {限制使用范围},
                "drugClass": {药品类别码},
                "outpatientFlag": {门诊统筹标志},
                "dataCreated": {创建日期},
                "medicareDrugId": {医保药品目录ID},
                "retirementPayRatio": {离休自付比例},
                "drugTradeName": {药品商品名},
                "chargeLevel": {收费项目等级},
                "medicine": {剂型},
                "medicalInstitutionNumber": {医疗机构编号},
                "baseDrugFlag": {国家基本药物标志},
                "packMaterial": {包装材质},
                "hospitalizedPayRatio": {住院自付比例},
                "majorFunctions": {主要适用症}
            }
        ],
        "pageable": {
            "sort": {
                "sorted": true,
                "unsorted": false,
                "empty": false
            },
            "offset": 0,
            "pageNumber": 0,
            "pageSize": 21,
            "unpaged": false,
            "paged": true
        },
        "totalElements": 1,
        "last": true,
        "totalPages": 1,
        "number": 0,
        "size": 21,
        "sort": {
            "sorted": true,
            "unsorted": false,
            "empty": false
        },
        "numberOfElements": 1,
        "first": true,
        "empty": false
    }
}
```

##### 2.6 重置已对应商品审批状态
请求地址：/web/medicareDrugsGoodsRel/resetApproved

请求参数：
```json
{
    "systemId":{系统ID}
}
```
响应参数：
```json
{
    {标准响应}
}
```

##### 2.7 医保对照新增（新）
请求地址：/web/medicareDrugsGoodsRel/saveNew

请求参数：
```json
{
    "medicareDrugId":{医保目录商品ID},
    "goodsId":{本地商品ID},
    "adminId":{用户ID},
    "companyId":{公司ID}
}
```
响应参数：
```json
{
    {标准响应}
}
```

##### 2.8 医保对照查询（新）
请求地址：/web/medicareDrugsGoodsRel/queryNew

请求参数：
```json
{
    "page": {
        "currentNum": 当前页,
        "pageSize": 每页条数
    },
    "goodsId":{本地商品ID},
    "drugName":{通用中文名称},
    "approvalNo":{国药准字},
    "drugCode":{医保商品编码}
}
```
响应参数：
```json
{
    "code": 0,
    "message": "操作成功",
    "data": {
        "content": [
            {
                "approvalNo": {国药准字},
                "productionUnit": {生产单位},
                "endDate": {终止时间/YYYYMMDDHH24MISS},
                "basicMedicalFlag": {基本医疗使用标志},
                "childMedicationFlag": {儿童用药标志},
                "memo": {备注},
                "outpatientPayRatio": {普通门诊自付比例},
                "medicareDrugsGoodsRelId": {关系表ID},
                "frequency": {使用频次},
                "lastUpdated": {修改日期},
                "handleDate": {经办日期},
                "drugSpecs": {规格},
                "countryCatalogCode": {国家目录编码},
                "minPackQty": {最小包装数量},
                "herbalManageMethod": {中草药管理办法},
                "priceCeiling": {最高限价},
                "drugDosageUnit": {药品剂量单位},
                "factoryName": {药厂名称},
                "limitedDay": {限定天数},
                "nationalDrugCode": {药监局药品编码},
                "prescriptionFlag": {处方药标志},
                "approvedDate": {批准日期},
                "fiveMnemonic": {五笔助记码},
                "unit": {单位},
                "companyId": {公司ID},
                "fertilityFlag": {生育使用标志},
                "pinyin": {拼音码},
                "registeredName": {药品注册名称},
                "drugName": {通用中文名称},
                "needApproval": {是否需要审批标志},
                "startDate": {开始时间},
                "dosage": {用法},
                "englishName": {英文名称},
                "chargeCategory": {收费类别/11--西药，12--中成药，13--中草药，15—蒙药},
                "drugCode": {药监局药品编码},
                "validFlag": {有效标志},
                "goodsId": {本地商品ID},
                "eachDosage": {每次用量},
                "workInjuryFlag": {工伤使用标志},
                "hospitalFlag": {院内制剂标志},
                "customCode": {自定义码},
                "approvalRemark": {批准文号备注},
                "limitScope": {限制使用范围},
                "drugClass": {药品类别码},
                "adminId": {登陆人ID},
                "outpatientFlag": {门诊统筹标志},
                "dataCreated": {创建日期},
                "medicareDrugId": {医保药品目录ID},
                "retirementPayRatio": {离休自付比例},
                "systemId": {系统ID},
                "drugTradeName": {药品商品名},
                "chargeLevel": {收费项目等级},
                "medicine": {剂型},
                "medicalInstitutionNumber": {医疗机构编号},
                "baseDrugFlag": {国家基本药物标志},
                "packMaterial": {包装材质},
                "apiUserId": {api 用户ID},
                "hospitalizedPayRatio": {住院自付比例},
                "majorFunctions": {主要适用症},
                "approved": {是否审批通过/0 未通过,1 已通过},
            }
        ],
        "pageable": {
            "sort": {
                "unsorted": false,
                "sorted": true,
                "empty": false
            },
            "offset": 2,
            "pageSize": 1,
            "pageNumber": 2,
            "paged": true,
            "unpaged": false
        },
        "last": true,
        "totalPages": 3,
        "totalElements": 3,
        "number": 2,
        "size": 1,
        "first": false,
        "sort": {
            "unsorted": false,
            "sorted": true,
            "empty": false
        },
        "numberOfElements": 1,
        "empty": false
    }
}
```


#### 3.软件系统信息
##### 3.1 新增
请求地址：/web/softwareSystem/save

请求参数：
```json
{
    "systemName": {系统名称},
    "adminId": {用户ID}
}
```
响应参数：
```json
{
    {标准响应}
}
```

##### 3.2修改
请求地址：/web/softwareSystem/update

请求参数：
```json
{
    "systemId":{系统ID},
    "systemName":{系统名称}
}
```
响应参数：
```json
{
    {标准响应}
}
```

##### 3.3删除
请求地址：/web/softwareSystem/delete

请求参数：
```json
{
    "ids":[{系统ID}]
}
```
响应参数：
```json
{
    {标准响应}
}
```

##### 3.4查询
请求地址：/web/softwareSystem/query

请求参数：
```json
{
    "adminId": {用户ID},
    "page": {
        "currentNum": 当前页,
        "pageSize": 每页数据条数
    }
}
```
响应参数：
```json
{
    "code": 0,
    "message": "操作成功",
    "data": {
        "content": [
            {
                "systemId": {系统ID},
                "systemName": {系统名称},
                "dataCreated": {录入日期},
                "lastUpdated": {修改日期}
            }
        ],
        "pageable": {
            "sort": {
                "sorted": true,
                "unsorted": false,
                "empty": false
            },
            "offset": 0,
            "pageNumber": 0,
            "pageSize": 20,
            "unpaged": false,
            "paged": true
        },
        "totalElements": 2,
        "last": true,
        "totalPages": 1,
        "number": 0,
        "size": 20,
        "sort": {
            "sorted": true,
            "unsorted": false,
            "empty": false
        },
        "numberOfElements": 2,
        "first": true,
        "empty": false
    }
}
```

#### 4.api接口
##### 4.1 grape产品查询商品医保信息
请求地址：/api/sinterface/queryMedicare

请求参数：
```json
{
    "systemId": {系统ID},
    "goodsIdList": [
        {商品ID},
        {商品ID}
    ]
}
```
响应参数：
```json
{
    "code": 0,
    "message": "操作成功",
    "data": [
        {
            "14428": {对应医保目录ID/可为null}
        }
    ]
}
```

##### 4.2 医保工具初始化
请求地址：/api/sinterface/init

请求参数：
```json
{
    "machine":{机器码}
}
```
响应参数：
```json
{
    "code": 0,
    "message": "操作成功",
    "data": {
        "eapagentConfigList": [
            {
                "eapagentConfigId": 1,
                "serviceUrl": {服务代理地址},
                "port": {服务代理端口},
                "hisHost": {医疗机构服务地址},
                "hisPort": {医疗机构服务端口}
            }
        ],
        "medicareConfigList": [
            {
                "medicareConfigId": {配置ID},
                "apiUserId": 3,
                "license": {医保 license},
                "cityNo": {当前盟市区号},
                "medicalInstitutionNo": {医疗机构编号},
                "centerCode": {中心编码},
            }
        ],
        "softwareSystemList": [
            {
                "systemId": {系统ID},
                "systemName": {系统名称},
                "apiUserId": {apiUserId}
            }
        ],
    }
}
```

##### 4.3 医保工具查询医保对应商品目录
请求地址：/api/sinterface/queryMedicareRel

请求参数：
```json
{
    "systemId": {软件系统ID},
    "approved":{是否审核通过，0未通过，1已通过},
    "page": {
        "currentNum": {当前页},
        "pageSize": {每页条数}
    }
}
```
响应参数：
```json
{
    "code": 0,
    "message": "操作成功",
    "data": {
        "content": [
            {
                "approvalNo": {国药准字},
                "productionUnit": {生产单位},
                "endDate": {终止时间/YYYYMMDDHH24MISS},
                "basicMedicalFlag": {基本医疗使用标志},
                "childMedicationFlag": {儿童用药标志},
                "memo": {备注},
                "outpatientPayRatio": {普通门诊自付比例},
                "medicareDrugsGoodsRelId": {关系表ID},
                "frequency": {使用频次},
                "lastUpdated": {修改日期},
                "handleDate": {经办日期},
                "drugSpecs": {规格},
                "countryCatalogCode": {国家目录编码},
                "minPackQty": {最小包装数量},
                "herbalManageMethod": {中草药管理办法},
                "priceCeiling": {最高限价},
                "drugDosageUnit": {药品剂量单位},
                "factoryName": {药厂名称},
                "limitedDay": {限定天数},
                "nationalDrugCode": {药监局药品编码},
                "prescriptionFlag": {处方药标志},
                "approvedDate": {批准日期},
                "fiveMnemonic": {五笔助记码},
                "unit": {单位},
                "companyId": {公司ID},
                "fertilityFlag": {生育使用标志},
                "pinyin": {拼音码},
                "registeredName": {药品注册名称},
                "drugName": {通用中文名称},
                "needApproval": {是否需要审批标志},
                "startDate": {开始时间},
                "dosage": {用法},
                "englishName": {英文名称},
                "chargeCategory": {收费类别/11--西药，12--中成药，13--中草药，15—蒙药},
                "drugCode": {药监局药品编码},
                "validFlag": {有效标志},
                "goodsId": {本地商品ID},
                "eachDosage": {每次用量},
                "workInjuryFlag": {工伤使用标志},
                "hospitalFlag": {院内制剂标志},
                "customCode": {自定义码},
                "approvalRemark": {批准文号备注},
                "limitScope": {限制使用范围},
                "drugClass": {药品类别码},
                "adminId": {登陆人ID},
                "outpatientFlag": {门诊统筹标志},
                "dataCreated": {创建日期},
                "medicareDrugId": {医保药品目录ID},
                "retirementPayRatio": {离休自付比例},
                "systemId": {系统ID},
                "drugTradeName": {药品商品名},
                "chargeLevel": {收费项目等级},
                "medicine": {剂型},
                "medicalInstitutionNumber": {医疗机构编号},
                "baseDrugFlag": {国家基本药物标志},
                "packMaterial": {包装材质},
                "apiUserId": {api 用户ID},
                "hospitalizedPayRatio": {住院自付比例},
                "majorFunctions": {主要适用症}
            }
        ],
        "pageable": {
            "sort": {
                "sorted": true,
                "unsorted": false,
                "empty": false
            },
            "offset": 0,
            "pageSize": 20,
            "pageNumber": 0,
            "paged": true,
            "unpaged": false
        },
        "totalElements": 1,
        "totalPages": 1,
        "last": true,
        "number": 0,
        "size": 20,
        "sort": {
            "sorted": true,
            "unsorted": false,
            "empty": false
        },
        "numberOfElements": 1,
        "first": true,
        "empty": false
    }
}
```

##### 4.4 医保工具更新商品审核结果（只更新通过审核信息）
请求地址：/api/sinterface/updateMedicareRel

请求参数：
```json
{
    "systemId":{软件系统ID},
    "medicareDrugId":{医保商品ID},
    "goodsId":{本地商品ID}
}
```
响应参数：
```json
{
    {标准响应}
}
```

##### 4.5 查询软件信息
请求地址：/api/sinterface/querySystem

请求参数：
```json
{}
```
响应参数：
```json
{
    "code": 0,
    "message": "操作成功",
    "data": [
        {
            "systemId": {系统ID},
            "systemName": {系统名称},
            "apiUserId": {apiUserId}
        }
    ]
}
```

##### 4.6 查询操作员信息
请求地址：/api/sinterface/queryOperator

请求参数：
```json
{}
```
响应参数：
```json
{
    "code": 0,
    "message": "操作成功",
    "data": {
        "apiOperatorId": {操作员ID},
        "apiUserId": {apiUserId},
        "operatorNo": {操作员编码},
    }
}
```

##### 4.7 查询医保工具最新版心信息
请求地址：/api/sinterface/getLatestVersion

请求参数：
```json
{}
```
响应参数：
```json
{
    "code": 0,
    "message": "操作成功",
    "data": {
        "toolVersionLogId": {版本信息ID},
        "versionNum": {版本号},
        "updateLog": {版本更新日志},
        "downloadUrl": {下载地址},
        "dataCreated": {录入日期},
        "lastUpdated": {修改日期}
    }
}
```

##### 4.8 查询当前用户可用功能
请求地址：/api/sinterface/queryToolFunc

请求参数：
{}
响应参数：
```json
{
    "code": 0,
    "message": "操作成功",
    "data": [
        {
            "apiUserId": {apiUserId},
            "lastUpdated": {修改日期},
            "funcName": {功能名称},
            "funcKey": {功能Key},
            "toolFuncId": {功能ID},
            "apiToolFuncRelId": {关系ID},
            "dataCreated": {录入日期},
            "status": {状态}
        }
    ]
}
```

##### 4.9 医保工具查询异地申报详情
请求地址：/api/sinterface/queryOffSiteDeclaration

请求参数：
```json
{
    "startDate": {查询开始时间},
    "endDate": {查询结束时间}
}
```
响应参数：
```json
{
    "code": 0,
    "message": "操作成功",
    "data": [
        {
            "rowNum": {行号},
            "medicalInstitutionRegion": {所属医疗机构区域编号},
            "medicalCategory": {医疗类别},
            "treatmentCategory": {待遇类别},
            "personTimes": {人次},
            "cash": {现金},
            "expenditure": {账户支出},
            "payExpense": {自付费用},
            "ownExpense": {自费费用},
            "overallExpense": {统筹费用},
            "largeExpense": {大额费用},
            "civilServantSubsidy": {公务员补贴},
            "hospitalPayAmount": {医院自付金额}
        }
    ]
}
```



#### 5.医疗机构信息配置

##### 5.1 新增
请求地址：/web/medicareConfig/save

请求参数：
```json
{
    "license":{医保 license},
    "leagueCity":{当前盟市编号},
    "medicalInstitutionNo":{医疗机构编号},
    "centerCode":{中心编码},
    "fixmedinsCode":{定点医疗机构编号},
    "fixmedinsName":{定点医疗机构名称},
    "opterType":{经办人类别/1-经办人；2-自助终端；3-移动终端},
    "opter":{经办人,按地方要求传入经办人/终端编号},
    "opterName":{经办人姓名,按地方要求传入经办人/终端编号},
    "mdtrtareaAdmvs":{就医地医保区划},
    "signIp":{签到Ip},
    "signMac":{签到 mac}
}
```
响应参数：
```json
{
    {标准响应}
}
```

##### 5.2 修改
请求地址：/web/medicareConfig/update

请求参数：
```json
{
    "medicareConfigId":{配置ID},
    "license":{医保 license},
    "leagueCity":{当前盟市编号},
    "medicalInstitutionNo":{医疗机构编号},
    "centerCode":{中心编码}
}
```
响应参数：
```json
{
    {标准响应}
}
```

##### 5.3 删除
请求地址：/web/medicareConfig/delete

请求参数：
```json
{
    "ids":[{配置ID}]
}
```
响应参数：
```json
{
    {标准响应}
}
```

##### 5.3 查询
请求地址：/web/medicareConfig/query

请求参数：
```json
{}
```
响应参数：
```json
{
    "code": 0,
    "message": "操作成功",
    "data": {
        "content": [
            {
                "medicareConfigId": {配置ID},
                "license":{医保 license},
                "leagueCity":{当前盟市编号},
                "medicalInstitutionNo":{医疗机构编号},
                "centerCode":{中心编码},
                "dataCreated": {录入日期},
                "lastUpdated": {修改日期}
            }
        ],
        "pageable": {
            "sort": {
                "sorted": true,
                "unsorted": false,
                "empty": false
            },
            "offset": 0,
            "pageNumber": 0,
            "pageSize": 20,
            "paged": true,
            "unpaged": false
        },
        "totalElements": 1,
        "last": true,
        "totalPages": 1,
        "number": 0,
        "size": 20,
        "sort": {
            "sorted": true,
            "unsorted": false,
            "empty": false
        },
        "numberOfElements": 1,
        "first": true,
        "empty": false
    }
}
```


#### 6. 支付方式

##### 6.1 新增
请求地址：/sys/payType/save

请求参数：
```json
{
    "payTypeName":{支付方式名称},
    "payTypeDesc":{描述}
}
```
响应参数：
```json
{
    {标准响应}
}
```

##### 6.2 修改
请求地址：/sys/payType/update

请求参数：
```json
{
    "payTypeId":{支付方式ID},
    "payTypeName":{支付方式名称},
    "payTypeDesc":{描述},
    "status":{是否启用}
}
```
响应参数：
```json
{
    {标准响应}
}
```

##### 6.3 删除
请求地址：/sys/payType/delete

请求参数：
```json
{
    "ids":[{支付方式ID}]
}
```
响应参数：
```json
{
    {标准响应}
}
```

##### 6.4 查询 
请求地址：/sys/payType/query

请求参数：
```json
{
    "payTypeName":{支付方式名称},
    "page": {
        "currentNum": 当前页,
        "pageSize": 每页条数
    }
}
```
响应参数：
```json
{
    "code": 0,
    "message": "操作成功",
    "data": {
        "content": [
            {
                "payTypeId": {支付方式ID},
                "payTypeName": {支付方式名称},
                "status": {是否启用/0:启用,1:停用},
                "payTypeDesc": {描述},
                "dataCreated": {录入日期},
                "lastUpdated": {修改日期}
            }
        ],
        "pageable": {
            "sort": {
                "sorted": true,
                "unsorted": false,
                "empty": false
            },
            "offset": 0,
            "pageNumber": 0,
            "pageSize": 1,
            "unpaged": false,
            "paged": true
        },
        "totalElements": 1,
        "last": true,
        "totalPages": 1,
        "number": 0,
        "size": 1,
        "sort": {
            "sorted": true,
            "unsorted": false,
            "empty": false
        },
        "numberOfElements": 1,
        "first": true,
        "empty": false
    }
}
```

##### 6.5 查询列表
请求地址：/web/payType/queryList

请求参数：
```json
{}
```
响应参数：
```json
{
    "code": 0,
    "message": "操作成功",
    "data": [
        {
            "payTypeId": {支付方式ID},
            "payTypeName": {支付方式名称},
            "status": {是否启用/0:启用,1:停用},
            "payTypeDesc": {描述},
            "dataCreated": {录入日期},
            "lastUpdated": {修改日期}
        }
    ]
}
```

#### 7. 支付年限配置表
##### 7.1 新增
请求地址：/sys/payPeriodConfig/save

请求参数：
```json
{
    "period":{年限},
    "amount":{金额}
}
```
响应参数：
```json
{
    {标准响应}
}
```

##### 7.2 修改
请求地址：/sys/payPeriodConfig/update

请求参数：
```json
{
    "payPeriodConfigId":{ID},
    "period":{年限},
    "amount":{金额}
}
```
响应参数：
```json
{
    {标准响应}
}
```
##### 7.3 删除
请求地址：/sys/payPeriodConfig/delete

请求参数：
```json
{
    "ids":[{ID}]
}
```
响应参数：
```json
{
    {标准响应}
}
```

##### 7.4 查询
请求地址：/sys/payPeriodConfig/query

请求参数：
```json
{
    "page": {
        "currentNum": 当前页,
        "pageSize": 每页条数
    }
}
```
响应参数：
```json
{
    "code": 0,
    "message": "操作成功",
    "data": {
        "content": [
            {
                "payPeriodConfigId": {ID},
                "period": {年限},
                "amount": {金额},
                "dataCreated": {录入日期},
                "lastUpdated": {修改日期}
            }
        ],
        "pageable": {
            "sort": {
                "sorted": true,
                "unsorted": false,
                "empty": false
            },
            "offset": 0,
            "pageNumber": 0,
            "pageSize": 1,
            "unpaged": false,
            "paged": true
        },
        "totalElements": 1,
        "last": true,
        "totalPages": 1,
        "number": 0,
        "size": 1,
        "sort": {
            "sorted": true,
            "unsorted": false,
            "empty": false
        },
        "numberOfElements": 1,
        "first": true,
        "empty": false
    }
}
```

##### 7.5 查询列表
请求地址：/web/payPeriodConfig/queryList

请求参数：
```json
{}
```
响应参数：
```json
{
    "code": 0,
    "message": "操作成功",
    "data": [
        {
            "payPeriodConfigId": {ID},
            "period": {年限},
            "amount": {金额},
            "dataCreated": {录入日期},
            "lastUpdated": {修改日期}
        }
    ]
}
```

#### 8. 支付订单信息
##### 8.1 查询
请求地址：/web/apiPaymentOrder/query

请求参数：
```json
{
    "page": {
        "currentNum": 当前页,
        "pageSize": 每页条数
    }
}
```
响应参数：
```json
{
    "code": 0,
    "message": "操作成功",
    "data": {
        "content": [
            {
                "apiPayOrderId": {订单ID},
                "payOrderNo": {订单编号},
                "apiUserId": {},
                "payTime": {支付时间},
                "payAmount": {支付金额},
                "payPeriod": {年限},
                "thirdTradeNo": {三方订单号},
                "payType": {支付方式/0:微信支付},
                "dataCreated": {录入日期},
                "lastUpdated": {修改日期}
            }
        ],
        "pageable": {
            "sort": {
                "sorted": true,
                "unsorted": false,
                "empty": false
            },
            "offset": 0,
            "pageNumber": 0,
            "pageSize": 1,
            "unpaged": false,
            "paged": true
        },
        "totalElements": 1,
        "totalPages": 1,
        "last": true,
        "number": 0,
        "size": 1,
        "sort": {
            "sorted": true,
            "unsorted": false,
            "empty": false
        },
        "numberOfElements": 1,
        "first": true,
        "empty": false
    }
}
```

##### 8.2 数据中心查询订单信息
请求地址：/sys/apiPaymentOrder/query

请求参数：
```json
{
    "page": {
        "currentNum": 当前页,
        "pageSize": 每页条数
    },
    "adminId":{用户ID}
}
```
响应参数：
```json
{
    "code": 0,
    "message": "操作成功",
    "data": {
        "content": [
            {
                "apiPayOrderId": {订单ID},
                "payOrderNo": {订单编号},
                "apiUserId": {},
                "payTime": {支付时间},
                "payAmount": {支付金额},
                "payPeriod": {年限},
                "thirdTradeNo": {三方订单号},
                "payType": {支付方式/0:微信支付},
                "dataCreated": {录入日期},
                "lastUpdated": {修改日期}
            }
        ],
        "pageable": {
            "sort": {
                "sorted": true,
                "unsorted": false,
                "empty": false
            },
            "offset": 0,
            "pageNumber": 0,
            "pageSize": 1,
            "unpaged": false,
            "paged": true
        },
        "totalElements": 1,
        "totalPages": 1,
        "last": true,
        "number": 0,
        "size": 1,
        "sort": {
            "sorted": true,
            "unsorted": false,
            "empty": false
        },
        "numberOfElements": 1,
        "first": true,
        "empty": false
    }
}
```

##### 8.3 延期新增订单信息
请求地址：/sys/apiPaymentOrder/save

请求参数：
```json
{
    "apiUserId":{},
    "payOrderNo":{订单编号},
    "payTime":{支付时间},
    "payAmount":{支付金额},
    "payType":{支付方式/0微信，1现金，2支付宝，3其他},
    "payPeriod":{支付年限单位年,不能填小数}
}
```
响应参数：
```json
{}
```

#### 9. 支付接口
##### 9.1 续费微信支付
请求地址：/web/pay/wxPay

请求参数：
```json
{
    "payPeriodConfigId":{支付年限配置ID}
}
```
响应参数：
```json
{
    "code": 0,
    "message": "操作成功",
    "data": {
        "codeUrl": {支付地址},
        "outTradeNo": {订单号}
    }
}
```

##### 9.2 查询支付结果
请求地址：/web/pay/checkWxPayStatus

请求参数：
```json
{
    "outTradeNo":{订单号}
}
```
响应参数：
```json
{
    {标准响应}
}
```

#### 10. 管理员

##### 10.1 登录
请求地址：/sys/sysUser/login

请求参数：
```json
{
    "loginName":{登录名称},
    "password":{登录密码},
    "captchaNo":{验证码ID},
    "code":{验证码}
}
```
响应参数：
```json
{
    "code": 0,
    "message": "操作成功",
    "data": {
        "sysUser": {
            "sysUserId": {管理员ID},
            "userName": {名称},
            "loginName": {登录名},
            "status": {状态/0启用，停用},
            "dataCreated": {录入日期},
            "lastUpdated": {修改日期}
        },
        "token": {token}
    }
}
```

##### 10.2 新增
请求地址：/sys/sysUser/save

请求参数：
```json
{
    "userName":{管理员名称},
    "loginName":{登录名称},
    "password":{登录密码}
}
```
响应参数：
```json
{
    {标准响应}
}
```

##### 10.3 修改
请求地址：/sys/sysUser/update

请求参数：
```json
{
    "userName":{管理员名称},
    "loginName":{登录名称},
    "password":{登录密码}
}
```
响应参数：
```json
{
    {标准响应}
}
```

##### 10.4 删除
请求地址：/sys/sysUser/delete

请求参数：
```json
{
    "ids":[{管理员ID}]
}
```
响应参数：
```json
{
    {标准响应}
}
```

##### 10.5 查询
请求地址：/sys/sysUser/query

请求参数：
```json
{
    "page": {
        "currentNum": 0,
        "pageSize": 20
    }
}
```
响应参数：
```json
{
    "code": 0,
    "message": "操作成功",
    "data": {
        "content": [
            {
                "sysUserId": {管理员ID},
                "userName": {管理员名称},
                "loginName": {登录名},
                "status": {状态/0启用，停用},
                "dataCreated": {录入日期},
                "lastUpdated": {修改日期}
            }
        ],
        "pageable": {
            "sort": {
                "sorted": true,
                "unsorted": false,
                "empty": false
            },
            "offset": 0,
            "pageSize": 20,
            "pageNumber": 0,
            "unpaged": false,
            "paged": true
        },
        "totalElements": 2,
        "totalPages": 1,
        "last": true,
        "number": 0,
        "size": 20,
        "sort": {
            "sorted": true,
            "unsorted": false,
            "empty": false
        },
        "numberOfElements": 2,
        "first": true,
        "empty": false
    }
}
```

#### 11 公司信息

##### 11.1 新增
请求地址：/sys/sysCompanyInfo/save

请求参数：
```json
{
    "companyName":{公司名称},
    "contactName":{公司联系人名称},
    "mobile":{联系电话}
}
```
响应参数：
```json
{
    {标准响应}
}
```

##### 11.2 修改
请求地址：/sys/sysCompanyInfo/update

请求参数：
```json
{
    "companyId":{公司ID},
    "companyName":{公司名称},
    "contactName":{公司联系人名称},
    "mobile":{联系电话}
}
```
响应参数：
```json
{
    {标准响应}
}
```

##### 11.3 查询
请求地址：/sys/sysCompanyInfo/query

请求参数：
```json
{
    "page": {
        "currentNum": 0,
        "pageSize": 20
    },
    "companyName":{公司名称}
}
```
响应参数：
```json
{
    {标准响应},
    "data": {
        "content": [
            {
                "companyId": {公司ID},
                "companyName": {公司名称},
                "contactName": {联系人名称},
                "mobile": {联系手机号},
                "dataCreated": {录入日期},
                "lastUpdated": {修改日期}
            }
        ],
        "pageable": {
            "sort": {
                "sorted": true,
                "unsorted": false,
                "empty": false
            },
            "offset": 0,
            "pageNumber": 0,
            "pageSize": 20,
            "paged": true,
            "unpaged": false
        },
        "totalElements": 1,
        "last": true,
        "totalPages": 1,
        "number": 0,
        "size": 20,
        "sort": {
            "sorted": true,
            "unsorted": false,
            "empty": false
        },
        "numberOfElements": 1,
        "first": true,
        "empty": false
    }
}
```
##### 11.4 查询列表
请求地址：/sys/sysCompanyInfo/queryList

请求参数：
```json
{}
```
响应参数：
```json
{
    "code": 0,
    "message": "操作成功",
    "data": [
        {
            "companyId": {公司ID},
            "companyName": {公司名称},
            "contactName": {联系人名称},
            "mobile": {联系手机号},
            "dataCreated": {录入日期},
            "lastUpdated": {修改日期}
        }
    ]
}
```

##### 11.5 查询下属信息
请求地址：/sys/sysApiUser/querySubordinate

请求参数：
```json
{
    "page": {
        "currentNum": 0,
        "pageSize": 20
    },
    "companyId": {公司ID},
    "fullName": {全名}
}
```
响应参数：
```json
{
    "code": 0,
    "message": "操作成功",
    "data": {
        "content": [
            {
                "registerTime": {注册时间},
                "mobile": {用户手机号},
                "fullName": {全名},
                "shopName": {门店名称},
                "apiUserId": {apiUserId},
                "expiryDate": {过期时间},
                "lastUpdated": {修改日期},
                "companyId": {公司Id},
                "loginName": {登录名},
                "adminId": {用户ID},
                "dataCreated": {创建日期},
                "apiId": {apiId},
                "status": {是否启用/0: 启用,1：停用}
            }
        ],
        "pageable": {
            "sort": {
                "sorted": false,
                "unsorted": true,
                "empty": true
            },
            "offset": 0,
            "pageNumber": 0,
            "pageSize": 20,
            "unpaged": false,
            "paged": true
        },
        "totalElements": 1,
        "totalPages": 1,
        "last": true,
        "number": 0,
        "size": 20,
        "sort": {
            "sorted": false,
            "unsorted": true,
            "empty": true
        },
        "numberOfElements": 1,
        "first": true,
        "empty": false
    }
}
```

##### 11.6 添加公司下属
请求地址：/sys/sysApiUser/addSubordinate

请求参数：
```json
{
    "apiUserId":{apiUserid},
    "companyId":{公司ID}
}
```
响应参数：
```json
{
    {标准响应}
}
```

##### 11.7 删除公司下属
请求地址：/sys/sysApiUser/delSubordinate

请求参数：
```json
{
    "apiUserId":{apiUserid},
    "companyId":{公司ID}
}
```
响应参数：
```json
{
    {标准响应}
}
```



#### 12 医保对照目录标准库
##### 12.1 查询
请求地址：/sys/sysMedicareGoodsRel/query

请求参数：
```json
{
    "page": {
        "currentNum": 0,
        "pageSize": 20
    },
    "drugName":{药品名称},
    "companyId":{公司ID},
    "goodsId":{本地商品ID},
    "approvalNo":{国药准字},
    "drugCode":{医保商品编码}
}
```
响应参数：
```json
{
    "code": 0,
    "message": "操作成功",
    "data": {
        "content": [
            {
                "approvalNo": {国药准字},
                "productionUnit": {生产单位},
                "endDate": {终止时间/YYYYMMDDHH24MISS},
                "basicMedicalFlag": {基本医疗使用标志},
                "childMedicationFlag": {儿童用药标志},
                "memo": {备注},
                "outpatientPayRatio": {普通门诊自付比例},
                "sysMedicareGoodsRelId": {关系表ID},
                "frequency": {使用频次},
                "lastUpdated": {修改日期},
                "handleDate": {经办日期},
                "drugSpecs": {规格},
                "countryCatalogCode": {国家目录编码},
                "minPackQty": {最小包装数量},
                "herbalManageMethod": {中草药管理办法},
                "priceCeiling": {最高限价},
                "drugDosageUnit": {药品剂量单位},
                "factoryName": {药厂名称},
                "limitedDay": {限定天数},
                "nationalDrugCode": {药监局药品编码},
                "prescriptionFlag": {处方药标志},
                "approvedDate": {批准日期},
                "fiveMnemonic": {五笔助记码},
                "unit": {单位},
                "companyId": {公司ID},
                "fertilityFlag": {生育使用标志},
                "pinyin": {拼音码},
                "registeredName": {药品注册名称},
                "drugName": {通用中文名称},
                "needApproval": {是否需要审批标志},
                "startDate": {开始时间},
                "dosage": {用法},
                "englishName": {英文名称},
                "chargeCategory": {收费类别/11--西药，12--中成药，13--中草药，15—蒙药},
                "drugCode": {药监局药品编码},
                "validFlag": {有效标志},
                "goodsId": {本地商品ID},
                "eachDosage": {每次用量},
                "workInjuryFlag": {工伤使用标志},
                "hospitalFlag": {院内制剂标志},
                "customCode": {自定义码},
                "approvalRemark": {批准文号备注},
                "limitScope": {限制使用范围},
                "drugClass": {药品类别码},
                "outpatientFlag": {门诊统筹标志},
                "dataCreated": {创建日期},
                "medicareDrugId": {医保药品目录ID},
                "retirementPayRatio": {离休自付比例},
                "drugTradeName": {药品商品名},
                "chargeLevel": {收费项目等级},
                "medicine": {剂型},
                "medicalInstitutionNumber": {医疗机构编号},
                "baseDrugFlag": {国家基本药物标志},
                "packMaterial": {包装材质},
                "hospitalizedPayRatio": {住院自付比例},
                "majorFunctions": {主要适用症},
                "price":{价格},
                "endDateTmp": {结束时间},
            }
        ],
        "pageable": {
            "sort": {
                "sorted": true,
                "unsorted": false,
                "empty": false
            },
            "offset": 0,
            "pageSize": 20,
            "pageNumber": 0,
            "paged": true,
            "unpaged": false
        },
        "totalElements": 1,
        "totalPages": 1,
        "last": true,
        "number": 0,
        "size": 20,
        "sort": {
            "sorted": true,
            "unsorted": false,
            "empty": false
        },
        "numberOfElements": 1,
        "first": true,
        "empty": false
    }
}
```

##### 12.2 新增
请求地址：/sys/sysMedicareGoodsRel/save

请求参数：
```json
{
    "companyId":{公司ID},
    "medicareDrugId":{医保目录ID},
    "goodsId":{本地商品ID}
}
```
响应参数：
```json
{
    {标准响应}
}
```

##### 12.3 修改
请求地址：/sys/sysMedicareGoodsRel/update

请求参数：
```json
{
    "sysMedicareGoodsRelId":{对照目录ID}
}
```
响应参数：
```json
{
    {标准响应}
}
```
##### 12.4 删除
请求地址：/sys/sysMedicareGoodsRel/delete

请求参数：
```json
{
    "ids":[{对照目录ID}]
}
```
响应参数：
```json
{
    {标准响应}
}
```

##### 12.5 初始化门店医保商品对照
请求地址：/sys/sysMedicareGoodsRel/initialize

请求参数：
```json
{
    "systemId":{软件ID/可为null},
    "adminId":{用户ID}
}
```

响应参数：
```json
{
    {标准响应}
}
```
##### 12.6 查询用户系统信息
请求地址：/sys/sysMedicareGoodsRel/querySoftware

请求参数：
```json
{
    "adminId":{用户ID}
}
```
响应参数：
```json
{
    "code": 0,
    "message": "操作成功",
    "data": [
        {
            "systemId": {系统ID},
            "systemName": {系统名称},
            "dataCreated": {录入日期},
            "lastUpdated": {修改日期}
        }
    ]
}
```

##### 12.7 使用Excel导入标准库
请求地址：/sys/sysMedicareGoodsRel/upload

请求参数：
```json
{
    "file":{Excel文件},
    "companyId":{公司ID}
}
```
响应参数：
```json
{
    {标准响应}
}
```


#### 13 代理服务配置
##### 13.1 新增
请求地址：/sys/eapagentConfig/save

请求参数：
```json
{
    "httpServer":{代理服务器地址},
    "port":{代理服务器接口},
    "hisHost":{医疗机构服务地址},
    "hisPort":{医疗机构服务端口},
    "offSiteHost":{异地服务地址},
    "offSitePort":{异地服务端口}
}
```
响应参数：
```json
{
    {标准响应}
}
```

##### 13.2 修改
请求地址：/sys/eapagentConfig/update

请求参数：
```json
{
    "eapagentConfigId":{配置ID},
    "httpServer":{代理服务器地址},
    "port":{代理服务器接口},
    "hisHost":{医疗机构服务地址},
    "hisPort":{医疗机构服务端口}
}
```
响应参数：
```json
{
    {标准响应}
}
```

##### 13.3 删除
请求地址：/sys/eapagentConfig/delete

请求参数：
```json
{
    "ids":[{配置ID}]
}
```
响应参数：
```json
{
    {标准响应}
}
```

##### 13.4 查询
请求地址：/sys/eapagentConfig/query

请求参数：
```json
{
    "page": {
        "currentNum": 0,
        "pageSize": 20
    }
}
```
响应参数：
```json
{
    "code": 0,
    "message": "操作成功",
    "data": {
        "content": [
            {
                "eapagentConfigId": {配置ID},
                "httpServer": {代理服务器地址},
                "port": {代理服务器接口},
                "hisHost": {医疗机构服务地址},
                "hisPort": {医疗机构服务端口},
                "offSiteHost":{异地服务地址},
                "offSitePort":{异地服务端口},
                "dataCreated": {录入日期},
                "lastUpdated": {修改日期}
            }
        ],
        "pageable": {
            "sort": {
                "sorted": true,
                "unsorted": false,
                "empty": false
            },
            "offset": 0,
            "pageSize": 20,
            "pageNumber": 0,
            "unpaged": false,
            "paged": true
        },
        "totalElements": 1,
        "last": true,
        "totalPages": 1,
        "number": 0,
        "size": 20,
        "sort": {
            "sorted": true,
            "unsorted": false,
            "empty": false
        },
        "numberOfElements": 1,
        "first": true,
        "empty": false
    }
}
```

#### 14 医保工具版本日志
##### 14.1 新增
请求地址：/sys/toolVersionLog/save

请求参数：
```json
{
    "versionNum":{版本号},
    "updateLog":{版本更新日志},
    "downloadUrl":{下载地址}
}
```
响应参数：
```json
{
    {标准响应}
}
```


##### 14.2 修改
请求地址：/sys/toolVersionLog/update

请求参数：
```json
{
    "toolVersionLogId":{版本信息ID},
    "versionNum":{版本号},
    "updateLog":{版本更新日志},
    "downloadUrl":{下载地址}
}
```
响应参数：
```json
{
    {标准响应}
}
```

##### 14.3 删除
请求地址：/sys/toolVersionLog/delete

请求参数：
```json
{
    "ids":[{版本信息ID}]
}
```
响应参数：
```json
{
    {标准响应}
}
```
##### 14.4 查询
请求地址：/sys/toolVersionLog/query

请求参数：
```json
{
    "page": {
        "currentNum": 0,
        "pageSize": 20
    }
}
```
响应参数：
```json
{
    "code": 0,
    "message": "操作成功",
    "data": {
        "content": [
            {
                "toolVersionLogId": {版本信息ID},
                "versionNum": {版本号},
                "updateLog": {版本更新日志},
                "downloadUrl": {下载地址},
                "dataCreated": {录入日期},
                "lastUpdated": {修改日期}
            }
        ],
        "pageable": {
            "sort": {
                "sorted": true,
                "unsorted": false,
                "empty": false
            },
            "offset": 0,
            "pageNumber": 0,
            "pageSize": 20,
            "unpaged": false,
            "paged": true
        },
        "totalElements": 1,
        "totalPages": 1,
        "last": true,
        "number": 0,
        "size": 20,
        "sort": {
            "sorted": true,
            "unsorted": false,
            "empty": false
        },
        "numberOfElements": 1,
        "first": true,
        "empty": false
    }
}
```

##### 14.5 开放接口查询医保工具版本信息
请求地址：/open/queryToolVersion

请求参数：
```json
{
    "page": {
        "currentNum": 0,
        "pageSize": 20
    }
}
```
响应参数：
```json
{
    "code": 0,
    "message": "操作成功",
    "data": {
        "content": [
            {
                "toolVersionLogId": {版本信息ID},
                "versionNum": {版本号},
                "updateLog": {版本更新日志},
                "downloadUrl": {下载地址},
                "dataCreated": {录入日期},
                "lastUpdated": {修改日期}
            }
        ],
        "pageable": {
            "sort": {
                "sorted": true,
                "unsorted": false,
                "empty": false
            },
            "offset": 0,
            "pageNumber": 0,
            "pageSize": 20,
            "unpaged": false,
            "paged": true
        },
        "totalElements": 1,
        "totalPages": 1,
        "last": true,
        "number": 0,
        "size": 20,
        "sort": {
            "sorted": true,
            "unsorted": false,
            "empty": false
        },
        "numberOfElements": 1,
        "first": true,
        "empty": false
    }
}
```


#### 15 开放接口
##### 15.1 获取验证码
请求地址：/open/getCaptCha

请求参数：
```json
{}
```
响应参数：
```json
{
    {标准响应},
    "data":{
        "captchaImgB6":{验证码图片base64码},
        "captchaNo":{验证码编号}
    }
}
```

#### 16 数据中心医保目录接口
##### 16.1 新增
请求地址：/sys/sysMedicareDrug/save

请求参数：
```json
{
    "approvalNo": {国药准字},
    "productionUnit": {生产单位},
    "endDate": {终止时间/YYYYMMDDHH24MISS},
    "basicMedicalFlag": {基本医疗使用标志},
    "childMedicationFlag": {儿童用药标志},
    "memo": {备注},
    "outpatientPayRatio": {普通门诊自付比例},
    "frequency": {使用频次},
    "lastUpdated": {修改日期},
    "handleDate": {经办日期},
    "drugSpecs": {规格},
    "countryCatalogCode": {国家目录编码},
    "minPackQty": {最小包装数量},
    "herbalManageMethod": {中草药管理办法},
    "priceCeiling": {最高限价},
    "drugDosageUnit": {药品剂量单位},
    "factoryName": {药厂名称},
    "limitedDay": {限定天数},
    "nationalDrugCode": {药监局药品编码},
    "prescriptionFlag": {处方药标志},
    "approvedDate": {批准日期},
    "fiveMnemonic": {五笔助记码},
    "unit": {单位},
    "companyId": {公司ID},
    "fertilityFlag": {生育使用标志},
    "pinyin": {拼音码},
    "registeredName": {药品注册名称},
    "drugName": {通用中文名称},
    "needApproval": {是否需要审批标志},
    "startDate": {开始时间},
    "dosage": {用法},
    "englishName": {英文名称},
    "chargeCategory": {收费类别/11--西药，12--中成药，13--中草药，15—蒙药},
    "drugCode": {药监局药品编码},
    "validFlag": {有效标志},
    "eachDosage": {每次用量},
    "workInjuryFlag": {工伤使用标志},
    "hospitalFlag": {院内制剂标志},
    "customCode": {自定义码},
    "approvalRemark": {批准文号备注},
    "limitScope": {限制使用范围},
    "drugClass": {药品类别码},
    "outpatientFlag": {门诊统筹标志},
    "dataCreated": {创建日期},
    "medicareDrugId": {医保药品目录ID},
    "retirementPayRatio": {离休自付比例},
    "drugTradeName": {药品商品名},
    "chargeLevel": {收费项目等级},
    "medicine": {剂型},
    "medicalInstitutionNumber": {医疗机构编号},
    "baseDrugFlag": {国家基本药物标志},
    "packMaterial": {包装材质},
    "hospitalizedPayRatio": {住院自付比例},
    "majorFunctions": {主要适用症}
}
```
响应参数：
```json
{
    {标准相应}
}
```

##### 16.2 修改
请求地址：/sys/sysMedicareDrug/update

请求参数：
```json
{
    "medicareDrugId": {医保药品目录ID}
}
```
响应参数：
```json
{
    {标准相应}
}
```

##### 16.3 删除
请求地址：/sys/sysMedicareDrug/delete

请求参数：
```json
{
    "ids":[{医保药品目录ID}]
}
```
响应参数：
```json
{
    {标准相应}
}
```

##### 16.4 查询
请求地址：/sys/sysMedicareDrug/query


请求参数：
```json
{
    "page": {
        "currentNum": 0,
        "pageSize": 20
    },
    "drugName":{药品名称},
    "approvalNo":{国药准字},
    "drugCode":{药品编码}
}
```
响应参数：
```json
{
    "code": 0,
    "message": "操作成功",
    "data": {
        "content": [
            {
                "approvalNo": {国药准字},
                "productionUnit": {生产单位},
                "endDate": {终止时间/YYYYMMDDHH24MISS},
                "basicMedicalFlag": {基本医疗使用标志},
                "childMedicationFlag": {儿童用药标志},
                "memo": {备注},
                "outpatientPayRatio": {普通门诊自付比例},
                "frequency": {使用频次},
                "lastUpdated": {修改日期},
                "handleDate": {经办日期},
                "drugSpecs": {规格},
                "countryCatalogCode": {国家目录编码},
                "minPackQty": {最小包装数量},
                "herbalManageMethod": {中草药管理办法},
                "priceCeiling": {最高限价},
                "drugDosageUnit": {药品剂量单位},
                "factoryName": {药厂名称},
                "limitedDay": {限定天数},
                "nationalDrugCode": {药监局药品编码},
                "prescriptionFlag": {处方药标志},
                "approvedDate": {批准日期},
                "fiveMnemonic": {五笔助记码},
                "unit": {单位},
                "companyId": {公司ID},
                "fertilityFlag": {生育使用标志},
                "pinyin": {拼音码},
                "registeredName": {药品注册名称},
                "drugName": {通用中文名称},
                "needApproval": {是否需要审批标志},
                "startDate": {开始时间},
                "dosage": {用法},
                "englishName": {英文名称},
                "chargeCategory": {收费类别/11--西药，12--中成药，13--中草药，15—蒙药},
                "drugCode": {药监局药品编码},
                "validFlag": {有效标志},
                "eachDosage": {每次用量},
                "workInjuryFlag": {工伤使用标志},
                "hospitalFlag": {院内制剂标志},
                "customCode": {自定义码},
                "approvalRemark": {批准文号备注},
                "limitScope": {限制使用范围},
                "drugClass": {药品类别码},
                "outpatientFlag": {门诊统筹标志},
                "dataCreated": {创建日期},
                "medicareDrugId": {医保药品目录ID},
                "retirementPayRatio": {离休自付比例},
                "drugTradeName": {药品商品名},
                "chargeLevel": {收费项目等级},
                "medicine": {剂型},
                "medicalInstitutionNumber": {医疗机构编号},
                "baseDrugFlag": {国家基本药物标志},
                "packMaterial": {包装材质},
                "hospitalizedPayRatio": {住院自付比例},
                "majorFunctions": {主要适用症}
            }
        ],
        "pageable": {
            "sort": {
                "sorted": true,
                "unsorted": false,
                "empty": false
            },
            "offset": 0,
            "pageNumber": 0,
            "pageSize": 21,
            "unpaged": false,
            "paged": true
        },
        "totalElements": 1,
        "last": true,
        "totalPages": 1,
        "number": 0,
        "size": 21,
        "sort": {
            "sorted": true,
            "unsorted": false,
            "empty": false
        },
        "numberOfElements": 1,
        "first": true,
        "empty": false
    }
}
```

##### 15.5 导入医保商品目录
请求地址：/sys/sysMedicareDrug/uploadMedicare

请求参数：
```json
{
    "file":{Excel文件}
}
```
响应参数：
```json
{
    {标准响应}
}
```

##### 16.5 Excel 上传医保商品目录
请求地址：/sys/sysMedicareDrug/upload

请求参数：
```json
{
    "file":{Excel文件}
}
```
响应参数:
```json
{
    {标准响应}
}
```

##### 16.6 根据医保商品编码删除对照关系与医保商品
请求地址：/sys/sysMedicareDrug/deleteDrugAndRel

请求参数：
```json
{
    "drugCodeList": [
        {医保商品编码},
        {医保商品编码}
    ]
}
```
响应参数：
```json
{
    {标准响应}
}
```


#### 17 医保工具功能
##### 17.1 新增
请求地址：/sys/toolFunc/save

请求参数：
```json
{
    "funcKey":{功能Key名称},
    "funcName":{功能名称}
}
```
响应参数：
```json
{
    {标准响应}
}
```

##### 17.2 修改
请求地址：/sys/toolFunc/update

请求参数：
```json
{
    "toolFuncId":{功能ID},
    "funcKey":{功能Key名称},
    "funcName":{功能名称},
    "status": {状态/0: 启用，1：停用},
}
```
响应参数：
```json
{
    {标准响应}
}
```

##### 17.3 删除
请求地址：/sys/toolFunc/delete

请求参数：
```json
{
    "ids":[{功能ID}]
}
```
响应参数：
```json
{
    {标准响应}
}
```

##### 17.4 查询
请求地址：/sys/toolFunc/query

请求参数：
```json
{
    "page": {
        "currentNum": 0,
        "pageSize": 20
    }
}
```
响应参数：
```json
{
    "code": 0,
    "message": "操作成功",
    "data": {
        "content": [
            {
                "toolFuncId": {功能ID},
                "funcKey": {功能Key},
                "funcName": {功能名称},
                "status": {状态/0: 启用，1：停用},
                "dataCreated": {录入日期},
                "lastUpdated": {修改日期}
            }
        ],
        "pageable": {
            "sort": {
                "sorted": true,
                "unsorted": false,
                "empty": false
            },
            "offset": 0,
            "pageSize": 20,
            "pageNumber": 0,
            "unpaged": false,
            "paged": true
        },
        "totalElements": 2,
        "totalPages": 1,
        "last": true,
        "number": 0,
        "size": 20,
        "sort": {
            "sorted": true,
            "unsorted": false,
            "empty": false
        },
        "numberOfElements": 2,
        "first": true,
        "empty": false
    }
}
```

#### 18 企业医保工具关系
##### 18.1 新增
请求地址：/sys/companyToolFuncRel/save

请求参数：
```json
{
    "companyId":{企业ID},
    "toolFuncId":{医保工具功能ID}
}
```
响应参数：
```json
{
    {标准响应}
}
```

##### 18.2 修改
请求地址：/sys/companyToolFuncRel/update

请求参数：
```json
{
    "companyToolFuncRelId":{关系ID},
    "companyId":{企业ID},
    "toolFuncId":{医保工具功能ID},
    "status":{状态/0 启用，1 停用}
}
```
响应参数：
```json
{
    {标准响应}
}
```

##### 18.3 删除
请求地址：/sys/companyToolFuncRel/delete

请求参数：
```json
{
    "ids":[{关系ID}]
}
```
响应参数：
```json
{
    {标准响应}
}
```

##### 18.4 查询
请求地址：/sys/companyToolFuncRel/query

请求参数：
```json
{
    "page": {
        "currentNum": 0,
        "pageSize": 20
    },
    "companyId":{企业ID/必传}
}
```
响应参数：
```json
{
    "code": 0,
    "message": "操作成功",
    "data": {
        "content": [
            {
                "lastUpdated": {修改日期},
                "funcName": {功能名称},
                "companyId": {企业ID},
                "companyToolFuncRelId": {关系ID},
                "toolFuncId": {医保功能ID},
                "dataCreated": {录入日期},
                "status":{状态/0 启用，1 停用}
            }
        ],
        "pageable": {
            "sort": {
                "sorted": false,
                "unsorted": true,
                "empty": true
            },
            "offset": 0,
            "pageSize": 20,
            "pageNumber": 0,
            "unpaged": false,
            "paged": true
        },
        "totalElements": 1,
        "totalPages": 1,
        "last": true,
        "number": 0,
        "size": 20,
        "sort": {
            "sorted": false,
            "unsorted": true,
            "empty": true
        },
        "numberOfElements": 1,
        "first": true,
        "empty": false
    }
}
```

##### 18.5 初始化医保功能
请求地址：/sys/companyToolFuncRel/init

请求参数：
```json
{
    "companyId":{企业ID},
    "apiUserId":{apiUserId}
}
```
响应参数：
```json
{
    {标准响应}
}
```

#### 19 apiUser 与医保工具功能
##### 19.1 新增
请求地址：/sys/apiToolFuncRel/save

请求参数：
```json
{
    "apiUserId":{apiUserId},
    "toolFuncId":{医保工具功能ID}
}
```
响应参数：
```json
{
    {标准响应}
}
```

##### 19.2 修改
请求地址：/sys/apiToolFuncRel/update

请求参数：
```json
{
    "apiToolFuncRelId":{关系ID},
    "apiUserId":{apiUserId},
    "toolFuncId":{医保工具功能ID},
    "status":{状态/0:启用,1:停用}
}
```
响应参数：
```json
{
    {标准响应}
}
```

##### 19.3 删除
请求地址：/sys/apiToolFuncRel/delete

请求参数：
```json
{
    "ids":[{关系ID}]
}
```
响应参数：
```json
{
    {标准响应}
}
```

##### 19.4 查询
请求地址：/sys/apiToolFuncRel/query

请求参数：
```json
{

}
```
响应参数：
```json
{
    "code": 0,
    "message": "操作成功",
    "data": {
        "content": [
            {
                "apiUserId": {apiUserId},
                "lastUpdated": {修改日期},
                "funcName": {医保功能名称},
                "toolFuncId": {医保功能ID},
                "apiToolFuncRelId": {关系ID},
                "dataCreated": {录入日期},
                "status":{状态/0:启用,1:停用}
            }
        ],
        "pageable": {
            "sort": {
                "sorted": false,
                "unsorted": true,
                "empty": true
            },
            "offset": 0,
            "pageSize": 20,
            "pageNumber": 0,
            "unpaged": false,
            "paged": true
        },
        "totalElements": 1,
        "last": true,
        "totalPages": 1,
        "number": 0,
        "size": 20,
        "sort": {
            "sorted": false,
            "unsorted": true,
            "empty": true
        },
        "numberOfElements": 1,
        "first": true,
        "empty": false
    }
}
```
#### 20 企业管理员
##### 20.1 新增
请求地址：/sys/companyAdmin/save

请求参数：
```json
{
    "fullName":{人员名称},
    "loginName":{登录名},
    "loginPwd":{密码},
    "companyId":{企业ID}
}
```
响应参数：
```json
{
    {标准响应}
}
```

##### 20.2 修改
请求地址：/sys/companyAdmin/update

请求参数：
```json
{
    "companyAdminId":{企业管理员ID}
}
```
响应参数：
```json
{
    {标准响应}
}
```

##### 20.3 删除
请求地址：/sys/companyAdmin/delete

请求参数：
```json
{
    "ids":[{企业管理员ID}]
}
```
响应参数：
```json
{
    {标准响应}
}
```

##### 20.4 查询
请求地址：/sys/companyAdmin/query

请求参数：
```json
{
    "page": {
        "currentNum": 0,
        "pageSize": 20
    },
    "fullName":{全名},
    "companyName":{公司名称},
    "loginName":{登录名}
}
```
响应参数：
```json
{
    "code": 0,
    "message": "操作成功",
    "data": {
        "content": [
            {
                "lastUpdated": {修改日期},
                "companyId": {公司ID},
                "companyAdminId": {企业管理员ID},
                "loginName": {登录名},
                "companyName": {公司名称},
                "fullName": {全名},
                "dataCreated": {创建日期},
                "status": {状态/0: 启用,1：停用}
            }
        ],
        "pageable": {
            "sort": {
                "sorted": false,
                "unsorted": true,
                "empty": true
            },
            "offset": 0,
            "pageSize": 20,
            "pageNumber": 0,
            "unpaged": false,
            "paged": true
        },
        "totalElements": 1,
        "totalPages": 1,
        "last": true,
        "number": 0,
        "size": 20,
        "sort": {
            "sorted": false,
            "unsorted": true,
            "empty": true
        },
        "numberOfElements": 1,
        "first": true,
        "empty": false
    }
}
```

#####  20.5 企业管理员登录
请求地址：/company/companyAdmin/login

请求参数：
```json
{
    "loginName":{登录名},
    "loginPwd":{密码}
}
```
响应参数：
```json
{
    "code": 0,
    "message": "操作成功",
    "data": {
        "companyAdmin": {
            "companyAdminId": {企业管理员ID},
            "loginName": {登录名},
            "fullName": {人员名称},
            "status": {状态},
            "companyId": {企业ID},
            "dataCreated": {创建日期},
            "lastUpdated": {修改日期}
        },
        "token": {token}
    }
}
```
##### 20.6 企业管理员查询企业信息
请求地址：/company/companyInfo/query

请求参数：
```json
{
    "page": {
        "currentNum": 0,
        "pageSize": 20
    }
}
```
响应参数：
```json
{
    "code": 0,
    "message": "操作成功",
    "data": {
        "content": [
            {
                "companyId": 1,
                "companyName": "第一个企业",
                "contactName": null,
                "mobile": null,
                "dataCreated": "2021-05-21 14:44:22",
                "lastUpdated": "2021-05-21 14:44:23"
            }
        ],
        "pageable": {
            "sort": {
                "sorted": true,
                "unsorted": false,
                "empty": false
            },
            "offset": 0,
            "pageSize": 20,
            "pageNumber": 0,
            "paged": true,
            "unpaged": false
        },
        "totalElements": 1,
        "last": true,
        "totalPages": 1,
        "number": 0,
        "size": 20,
        "sort": {
            "sorted": true,
            "unsorted": false,
            "empty": false
        },
        "numberOfElements": 1,
        "first": true,
        "empty": false
    }
}
```

##### 20.6 企业管理员修改密码
请求地址：/company/companyAdmin/updatePwd

请求参数：
```json
{
    "loginPwd":{原密码},
    "resetPwd":{新密码}
}
```
响应参数：
```json
{
    {标准响应}
}
```

#### 21 企业医保目录
##### 21.1 查询
请求地址：/company/sysMedicareGoodsRel/query

请求参数：
```json
{
    "page": {
        "currentNum": 0,
        "pageSize": 20
    },
    "drugName":{药品名称},
    "goodsId":{本地商品ID},
    "approvalNo":{国药准字},
    "drugCode":{医保商品编码}
}
```
响应参数：
```json
{
    "code": 0,
    "message": "操作成功",
    "data": {
        "content": [
            {
                "approvalNo": {国药准字},
                "productionUnit": {生产单位},
                "endDate": {终止时间/YYYYMMDDHH24MISS},
                "basicMedicalFlag": {基本医疗使用标志},
                "childMedicationFlag": {儿童用药标志},
                "memo": {备注},
                "outpatientPayRatio": {普通门诊自付比例},
                "sysMedicareGoodsRelId": {关系表ID},
                "frequency": {使用频次},
                "lastUpdated": {修改日期},
                "handleDate": {经办日期},
                "drugSpecs": {规格},
                "countryCatalogCode": {国家目录编码},
                "minPackQty": {最小包装数量},
                "herbalManageMethod": {中草药管理办法},
                "priceCeiling": {最高限价},
                "drugDosageUnit": {药品剂量单位},
                "factoryName": {药厂名称},
                "limitedDay": {限定天数},
                "nationalDrugCode": {药监局药品编码},
                "prescriptionFlag": {处方药标志},
                "approvedDate": {批准日期},
                "fiveMnemonic": {五笔助记码},
                "unit": {单位},
                "companyId": {公司ID},
                "fertilityFlag": {生育使用标志},
                "pinyin": {拼音码},
                "registeredName": {药品注册名称},
                "drugName": {通用中文名称},
                "needApproval": {是否需要审批标志},
                "startDate": {开始时间},
                "dosage": {用法},
                "englishName": {英文名称},
                "chargeCategory": {收费类别/11--西药，12--中成药，13--中草药，15—蒙药},
                "drugCode": {药监局药品编码},
                "validFlag": {有效标志},
                "goodsId": {本地商品ID},
                "eachDosage": {每次用量},
                "workInjuryFlag": {工伤使用标志},
                "hospitalFlag": {院内制剂标志},
                "customCode": {自定义码},
                "approvalRemark": {批准文号备注},
                "limitScope": {限制使用范围},
                "drugClass": {药品类别码},
                "outpatientFlag": {门诊统筹标志},
                "dataCreated": {创建日期},
                "medicareDrugId": {医保药品目录ID},
                "retirementPayRatio": {离休自付比例},
                "drugTradeName": {药品商品名},
                "chargeLevel": {收费项目等级},
                "medicine": {剂型},
                "medicalInstitutionNumber": {医疗机构编号},
                "baseDrugFlag": {国家基本药物标志},
                "packMaterial": {包装材质},
                "hospitalizedPayRatio": {住院自付比例},
                "majorFunctions": {主要适用症},
                "price":{价格}
            }
        ],
        "pageable": {
            "sort": {
                "sorted": true,
                "unsorted": false,
                "empty": false
            },
            "offset": 0,
            "pageSize": 20,
            "pageNumber": 0,
            "paged": true,
            "unpaged": false
        },
        "totalElements": 1,
        "totalPages": 1,
        "last": true,
        "number": 0,
        "size": 20,
        "sort": {
            "sorted": true,
            "unsorted": false,
            "empty": false
        },
        "numberOfElements": 1,
        "first": true,
        "empty": false
    }
}
```

##### 21.2 新增
请求地址：/company/sysMedicareGoodsRel/save

请求参数：
```json
{
    "medicareDrugId":{医保目录ID},
    "goodsId":{本地商品ID}
}
```
响应参数：
```json
{
    {标准响应}
}
```

##### 21.3 修改
请求地址：/company/sysMedicareGoodsRel/update

请求参数：
```json
{
    "sysMedicareGoodsRelId":{对照目录ID}
}
```
响应参数：
```json
{
    {标准响应}
}
```
##### 21.4 删除
请求地址：/company/sysMedicareGoodsRel/delete

请求参数：
```json
{
    "ids":[{对照目录ID}]
}
```
响应参数：
```json
{
    {标准响应}
}
```

##### 21.5 初始化门店医保商品对照
请求地址：/company/sysMedicareGoodsRel/initialize

请求参数：
```json
{
    "systemId":{软件ID/可为null},
    "adminId":{用户ID}
}
```

响应参数：
```json
{
    {标准响应}
}
```
##### 21.6 查询用户系统信息
请求地址：/company/sysMedicareGoodsRel/querySoftware

请求参数：
```json
{
    "adminId":{用户ID}
}
```
响应参数：
```json
{
    "code": 0,
    "message": "操作成功",
    "data": [
        {
            "systemId": {系统ID},
            "systemName": {系统名称},
            "dataCreated": {录入日期},
            "lastUpdated": {修改日期}
        }
    ]
}
```

##### 21.7 使用Excel导入标准库
请求地址：/company/sysMedicareGoodsRel/upload

请求参数：
```json
{
    "file":{Excel文件},
    "companyId":{公司ID}
}
```
响应参数：
```json
{
    {标准响应}
}
```

#### 22 企业管理员操作下属
##### 22.1 查询下属
请求地址：/company/apiUser/querySubordinate

请求参数：
```json
{
    "page": {
        "currentNum": 0,
        "pageSize": 20
    },
    "fullName": {全名},
    "loginName":{登录名}
}
```
响应参数：
```json
{
    "code": 0,
    "message": "操作成功",
    "data": {
        "content": [
            {
                "registerTime": {注册时间},
                "mobile": {用户手机号},
                "fullName": {全名},
                "shopName": {门店名称},
                "apiUserId": {apiUserId},
                "expiryDate": {过期时间},
                "lastUpdated": {修改日期},
                "companyId": {公司Id},
                "loginName": {登录名},
                "adminId": {用户ID},
                "dataCreated": {创建日期},
                "apiId": {apiId},
                "status": {是否启用/0: 启用,1：停用}
            }
        ],
        "pageable": {
            "sort": {
                "sorted": false,
                "unsorted": true,
                "empty": true
            },
            "offset": 0,
            "pageNumber": 0,
            "pageSize": 20,
            "unpaged": false,
            "paged": true
        },
        "totalElements": 1,
        "totalPages": 1,
        "last": true,
        "number": 0,
        "size": 20,
        "sort": {
            "sorted": false,
            "unsorted": true,
            "empty": true
        },
        "numberOfElements": 1,
        "first": true,
        "empty": false
    }
}
```

##### 22.2 添加下属
请求地址：/company/apiUser/addSubordinate

请求参数：
```json
{
    "apiUserId":{apiUserid}
}
```
响应参数：
```json
{
    {标准响应}
}
```

##### 22.3 删除下属
请求地址：/company/apiUser/delSubordinate

请求参数：
```json
{
    "apiUserId":{apiUserid}
}
```
响应参数：
```json
{
    {标准响应}
}
```

##### 22.4 添加前查询人员
请求地址：/company/apiUser/queryList

请求参数：
```json
{
    "loginName":{登录名称},
    "page": {
        "currentNum": 0,
        "pageSize": 20
    }
}
```
响应参数：
```json
{
    "code": 0,
    "message": "操作成功",
    "data": {
        "content": [
            {
                "adminId": {用户ID},
                "loginName": {登录名称},
                "registerTime": {注册时间},
                "status": {状态/0: 启用,1：停用},
                "apiUserId": {apiUserId},
                "mobile": {手机号},
                "dataCreated": {录入日期},
                "lastUpdated": {修改日期}
            }
        ],
        "pageable": {
            "sort": {
                "sorted": true,
                "unsorted": false,
                "empty": false
            },
            "offset": 0,
            "pageNumber": 0,
            "pageSize": 20,
            "unpaged": false,
            "paged": true
        },
        "totalElements": 1,
        "last": true,
        "totalPages": 1,
        "number": 0,
        "size": 20,
        "sort": {
            "sorted": true,
            "unsorted": false,
            "empty": false
        },
        "numberOfElements": 1,
        "first": true,
        "empty": false
    }
}
```

#### 23 数据中心续费支付记录
##### 23.1 新增
请求地址：/sys/paymentRecord/save

请求参数：
```json
{
    "payMan":{支付人},
    "orderNo":{订单号},
    "payAmount":{支付金额},
    "payType":{支付渠道}
}
```
响应参数：
```json
{
    {标准响应}
}
```

##### 23.2 修改
请求地址：/sys/paymentRecord/update

请求参数：
```json
{
    "paymentRecordId":{续费记录ID},
    "payMan":{支付人},
    "orderNo":{订单号},
    "payAmount":{支付金额},
    "payType":{支付渠道}
}
```
响应参数：
```json
{
    {标准响应}
}
```

##### 23.3 删除
请求地址：/sys/paymentRecord/delete

请求参数：
```json
{
    "ids":[{续费记录ID}]
}
```
响应参数：
```json
{
    {标准响应}
}
```

##### 23.4 查询
请求地址：/sys/paymentRecord/query

请求参数：
```json
{
    "payMan":{支付人},
    "payType":{支付渠道},
    "page": {
        "currentNum": 0,
        "pageSize": 20
    }
}
```
响应参数：
```json
{
    "code": 0,
    "message": "操作成功",
    "data": {
        "content": [
            {
                "paymentRecordId":{续费记录ID},
                "payMan":{支付人},
                "orderNo":{订单号},
                "payAmount":{支付金额},
                "payType":{支付渠道},
                "dataCreated":{录入日期},
                "lastUpdated":{修改日期}
            }
        ],
        "pageable": {
            "sort": {
                "sorted": true,
                "unsorted": false,
                "empty": false
            },
            "offset": 0,
            "pageNumber": 0,
            "pageSize": 20,
            "unpaged": false,
            "paged": true
        },
        "totalElements": 1,
        "last": true,
        "totalPages": 1,
        "number": 0,
        "size": 20,
        "sort": {
            "sorted": true,
            "unsorted": false,
            "empty": false
        },
        "numberOfElements": 1,
        "first": true,
        "empty": false
    }
}
```

#### 24 异地查询
##### 24.1 查询异地就医医疗保险费用月明细表（药店）
请求地址：/web/offSite/queryOffSiteMonthDetail

请求参数：
```json
{
    "startDate":{开始时间},
    "endDate":{结束时间}
}
```
响应参数：
```json
{
    "code": 0,
    "message": "操作成功",
    "data": {
        "offSiteMonthDetail": [
            {
                "rowNum": {序号},
                "handleMan": {经办人},
                "coordinationAreaNum": {所属统筹区号},
                "pharmacyNum": {药店号},
                "receiptNum": {收据号},
                "medicalNum": {医疗保险号},
                "manName": {姓名},
                "cashPayment": {现金支付},
                "accountExpenditure": {账户支出},
                "overallExpenditure": {统筹支出},
                "largeExpenditure": {大额支出},
                "civilServantSubsidy": {公务员补助}
            },
        ]
    }
}
```

##### 24.2 查询自治区本级医疗保险定点医疗机构月结结算单（异地就医）
请求地址：/web/offSite/queryMonthlyStatement

请求参数：
```json
{
    "startDate":{开始时间},
    "endDate":{结束时间}
}
```
响应参数：
```json
{
    "code": 0,
    "message": "操作成功",
    "data": {
        "monthlyStatement": [
            {
                "rowNum": {序号},
                "medicalCategory": {医疗类别},
                "personnelCategory": {人员类别},
                "coordinationAreaNum": {所属统筹区号},
                "personTimes": {人次},
                "totalAmount": null,
                "accountPayment": {账户支付},
                "cashPayment": 现金支付,
                "overallPayment": {统筹支付},
                "largePayment": {大额支付},
                "civilServantSubsidy": {公务员补助}
            },
        ]
    }
}
```

##### 24.3 导出异地就医医疗保险费用月明细表（药店）
请求地址：/web/offSite/downloadOffSiteMonthDetail

请求参数：
```json
{
    "token":{token},
    "startDate":{开始时间},
    "endDate":{结束时间}
}
```
响应参数：
```json
{
    {标准响应}
}
```

##### 24.4 导出自治区本级医疗保险定点医疗机构月结结算单（异地就医）
请求地址：/web/offSite/downloadMonthlyStatement

请求参数：
```json
{
    "token":{token},
    "startDate":{开始时间},
    "endDate":{结束时间}
}
```
响应参数：
```json
{
    {标准响应}
}
```

##### 24.5 查询药店对账单
请求地址：/web/offSite/getShopStatement

请求参数：
```json
{
    "startDate":{开始时间},
    "endDate":{结束时间}
}
```
响应参数：
```json
{
    "code": 0,
    "message": "操作成功",
    "data": {
        "rowNum": {序号},
        "personTimes": {人次},
        "totalAmount": {总金额},
        "overallPayment": {统筹支付},
        "socialAssistancePayment": {社会医疗救助支付},
        "civilServantPayment": {公务员支付},
        "accountPayment": {账户支付},
        "cashPayment": {现金支付},
        "fixmedinsCode": {定点编号},
        "printDate": {打印日期},
        "manName": {负责人},
        "fixmedinsName": {定点医疗机构名称},
        "startDate": {开始日期},
        "endDate": {结束日期}
    }
}
```

##### 24.6 数据中心下载药店对账单(get)
请求地址：/sys/offSite/downloadShopStatement

请求参数：
```json
{
    "startDate":{开始日期},
    "endDate":{结束日期},
    "allFlag":{是否查询全部标志/0否，1是},
    "apiUserIdList":{apiUserId/数组}
}
```
响应参数：
```json
```

#### 25 用户端医保新接口查询商品目录
##### 25.1 医保西药目录查询
请求地址：/web/medicareDrug/queryMedicareWest

请求参数：
```json
{
    "drugName":{药品名称},
    "approvalNo":{批准文号},
    "drugCode":{药品编码},
}
```
响应参数：
西药中成药
```json
{
    "code": 0,
    "message": "操作成功",
    "data": {
        "content": [
            {
                "approvalNo": {批准文号},
                "factoryCode": {生产企业编号},
                "commonName": {药品通用名},
                "approvalNoEndDate": {批准文号结束日期},
                "basMednName": {基本药物标志名称},
                "chemicalName": {化学名称},
                "endDate": {结束日期},
                "limitUseScope": {限制使用范围},
                "drugCategory": {药品类别},
                "approvalNoStartDate": {批准文号开始日期},
                "frequency": {使用频次},
                "lastUpdated": null,
                "spDrugFlag": {特殊药品标志/0否,1是},
                "drugExpiryDate": {药品有效期},
                "lmtUsedFlag": {限制使用标志/1是,0否},
                "medicareCatalogId": {目录ID},
                "genericDrug": {仿制药一致性评价药品},
                "anotherName": {别名},
                "drugSpecs": {规格},
                "drugCategoryName": {药品类别名称},
                "nationDosageForm": {国家医保药品目录剂型},
                "uniqueNum": {唯一记录号},
                "talkDrugName": {医保谈判药品名称},
                "packMaterialName": {包装材质名称},
                "registrationNumStartDate": {药品注册证号开始日期},
                "ver": {版本号},
                "otcFlagName": {非处方药标志名称},
                "dosageForm": {药品剂型},
                "factoryName": {生产企业名称},
                "verName": {版本名称},
                "drugSpecsCode": {药品规格代码},
                "nationMedicalRemark": {国家医保药品目录备注},
                "registrationNumEndDate": {药品注册证号结束日期},
                "commonNameNum": {通用名编号},
                "registeredName": {注册名称},
                "nationCategoryFlag": {国家医保药品目录甲乙类标识},
                "pinyin": {拼音助记码},
                "drugName": {药品商品名},
                "packQty": {包装数量},
                "registeredDosaForm": {注册剂型},
                "startDate": {开始日期},
                "dosage": {每次用量},
                "dosageFormName": {药品剂型名称},
                "englishName": {英文名称},
                "categroyType": {目录类别},
                "updateDate": {更新时间},
                "drugCode": {医疗目录编码},
                "validFlag": {有效标志/0无效,1有效} ,
                "remark": {备注},
                "marketStateName": {市场状态名称},
                "standardCode": {药监本位码},
                "nationDrugCode": {国家药品编号},
                "packSpecs": {包装规格},
                "dataCreated": "2021-10-16 09:54:36",
                "spLmtpricDrugFlag": {特殊限价药品标志/0否,1是},
                "medicareDrugId": 14437,
                "basMednFlag": {基本药物标志/1是,2否},
                "registeredSpecs": {注册规格},
                "registrationNum": {药品注册证号},
                "medicareWestDrugId": 1,
                "marketState": {市场状态},
                "packMaterial": {包装材质},
                "otcFlag": {非处方药标志},
                "healthCommissionDrugCode": {卫健委药品编码},
                "tcmpatFlag": {中成药标志/1	是	2	否},
                "hiNegoDrugFlag": {医保谈判药品标志},
                "registeredDrugSpecsCode": {注册规格代码}
            }
        ],
        "pageable": {
            "sort": {
                "sorted": true,
                "unsorted": false,
                "empty": false
            },
            "offset": 0,
            "pageNumber": 0,
            "pageSize": 20,
            "unpaged": false,
            "paged": true
        },
        "totalElements": 1,
        "totalPages": 1,
        "last": true,
        "number": 0,
        "size": 20,
        "sort": {
            "sorted": true,
            "unsorted": false,
            "empty": false
        },
        "numberOfElements": 1,
        "first": true,
        "empty": false
    }
}
```
##### 25.2 医保中药饮片目录查询
请求地址：/web/medicareDrug/queryMedicareChn

请求参数：
```json
{
    "drugName":{药品名称},
    "drugCode":{药品编码},
}
```
响应参数：
```json
{
    "code": 0,
    "message": "操作成功",
    "data": {
        "content": [
            {
                "safeMeasure": {安全计量},
                "categroyType": {目录类别},
                "updateDate": {更新时间},
                "drugCode": {医疗目录编码},
                "endDate": {结束日期},
                "validFlag": {有效标志/0无效,1有效},
                "processMethod": {炮制方法},
                "provincePayPolicy": {省级医保支付政策},
                "effectClass": {功效分类},
                "lastUpdated": null,
                "medicareCatalogId": {目录ID},
                "variety": {品种},
                "qualityLeve": {质量等级},
                "dataCreated": "2021-10-16 09:54:00",
                "uniqueNum": {唯一记录号},
                "dataUpdateTime": {数据更新时间},
                "materialSource": {药材种来源},
                "medicareDrugId": {医保目录ID},
                "medicinalTaste": {性味},
                "standardName": {标准名称},
                "ver": {版本号},
                "medicareChnDrugId": 1,
                "standardPage": {标准页码},
                "dataCreatedTime": {数据创建时间},
                "generalUsage": {常规用法},
                "verName": {版本名称},
                "nationPayPolicy": {国家医保支付政策},
                "materialName": {药材名称},
                "attribution": {归经},
                "drugName": {单味药名称},
                "singleFlag": {单复方标志},
                "indication": {功能主治},
                "medicinalPart": {药用部位},
                "standardElecFile": {标准电子档案},
                "startDate": {开始日期},
                "chnYear": {中草药年份}
            }
        ],
        "pageable": {
            "sort": {
                "sorted": true,
                "unsorted": false,
                "empty": false
            },
            "offset": 0,
            "pageSize": 20,
            "pageNumber": 0,
            "unpaged": false,
            "paged": true
        },
        "totalElements": 1,
        "totalPages": 1,
        "last": true,
        "number": 0,
        "size": 20,
        "sort": {
            "sorted": true,
            "unsorted": false,
            "empty": false
        },
        "numberOfElements": 1,
        "first": true,
        "empty": false
    }
}
```
##### 25.3 医保医用耗材目录查询
请求地址：/web/medicareDrug/queryConsumable

请求参数：
```json
{
    "drugName":{药品名称},
    "drugCode":{药品编码},
}
```
响应参数：
```json
{
    "code": 0,
    "message": "操作成功",
    "data": {
        "content": [
            {
                "factoryCode": {生产企业编号},
                "deviceUniqueNum": {医疗器械唯一标识码},
                "proNum": {产品型号},
                "caseFlag": {机套标志/1是,2否},
                "endDate": {结束日期},
                "packUnit": {包装单位},
                "registeredPsnName": {注册备案人名称},
                "genericName": {医保通用名},
                "picNum": {产品图片编号},
                "Instruction": {产品使用方法},
                "proStandard": {产品标准},
                "specsModel": {规格型号},
                "qualityStandard": {产品质量标准},
                "lastUpdated": null,
                "lmtUsedFlag": {限制使用标志/1是,0否},
                "productImage": {产品影像},
                "medicareCatalogId": {目录ID},
                "consumClass": {耗材分类},
                "minsalunt": {最小销售单位},
                "uniqueNum": {唯一记录号},
                "specialMachinename": {专机名称},
                "strucPart": {结构及组成},
                "ver": 版本号,
                "factoryName": {生产企业名称},
                "verName": {版本名称},
                "perfStrucPart": {性能结构与组成},
                "dspoUsedFlag": {一次性使用标志/1是,2否},
                "imptMatlHmorgnFlag": {植入材料和人体器官标志/1是,2否},
                "genericNameCode": {医保通用名代码},
                "approvedDate": {批准日期},
                "deviceCategory": {医疗器械管理类别},
                "smallestUseUnit": {最小使用单位},
                "proExpiryDate": {产品有效期},
                "imptItvtClssFlag": {植入或介入类标志/0	否,1植入,2介入},
                "registeredPsnAds": {注册备案人住所},
                "drugName": {耗材名称},
                "restrictedUserFlag": {医保限用范围},
                "packQty": {包装数量},
                "specialFlag": {专机专用标志},
                "sonsumSpecs": {规格},
                "startDate": {开始日期},
                "setName": 组套名称,
                "categroyType": {目录类别},
                "updateDate": {更新时间},
                "drugCode": {医疗目录编码},
                "elecFile": {注册或备案证电子档案},
                "validFlag": {有效标志/0无效,1有效},
                "agentEnterprise": {代理人企业},
                "registrationName": {注册备案产品名称},
                "dspoUsedName": {一次性使用标志名称},
                "prodAreaTypeName": {生产地类别名称},
                "consumSpecsCode": {规格代码},
                "highvalMcsFlag": {高值耗材标志/1是,2否},
                "manual": {说明书},
                "otherSupportMaterial": {其他证明材料},
                "medicalMaterialClassCode": {医用材料分类代码},
                "sterilizatioName": {灭菌标志名称},
                "imptItvtClssName": {植入或介入类名称},
                "consumMaterial": {耗材材质},
                "packSpecs": {包装规格},
                "scope": {适用范围},
                "dataCreated": "2021-10-16 09:49:07",
                "proAds": {生产地址},
                "certEndTime": {注册证有效期结束时间},
                "medicareDrugId": {医保目录ID},
                "conversionRatio": {产品转换比},
                "medicareConsumableId": {医用耗材ID},
                "certStartTime": {注册证有效期开始时间},
                "proCountryOrRegion": {生产国或地区},
                "registrationNum": {注册备案号},
                "sterilizationFlag": {灭菌标志},
                "afterSalesAgency": 售后服务机构,
                "materialCode": {材质代码},
                "agentEnterpriseAds": {代理人企业地址},
                "packMaterial": {产品包装材质},
                "prodAreaType": {生产地类别},
                "otherContent": {其他内容},
                "deviceCategoryName": {医疗器械管理类别名称}
            }
        ],
        "pageable": {
            "sort": {
                "sorted": true,
                "unsorted": false,
                "empty": false
            },
            "offset": 0,
            "pageNumber": 0,
            "pageSize": 20,
            "unpaged": false,
            "paged": true
        },
        "totalElements": 2,
        "totalPages": 1,
        "last": true,
        "number": 0,
        "size": 20,
        "sort": {
            "sorted": true,
            "unsorted": false,
            "empty": false
        },
        "numberOfElements": 2,
        "first": true,
        "empty": false
    }
}
```

#### 26 服务端医保新接口查询商品目录
##### 26.1 医保西药目录查询
请求地址：/sys/sysMedicareDrug/queryMedicareWest

请求参数：
```json
{
    "drugName":{药品名称},
    "approvalNo":{批准文号},
    "drugCode":{药品编码},
}
```
响应参数：
西药中成药
```json
{
    "code": 0,
    "message": "操作成功",
    "data": {
        "content": [
            {
                "approvalNo": {批准文号},
                "factoryCode": {生产企业编号},
                "commonName": {药品通用名},
                "approvalNoEndDate": {批准文号结束日期},
                "basMednName": {基本药物标志名称},
                "chemicalName": {化学名称},
                "endDate": {结束日期},
                "limitUseScope": {限制使用范围},
                "drugCategory": {药品类别},
                "approvalNoStartDate": {批准文号开始日期},
                "frequency": {使用频次},
                "lastUpdated": null,
                "spDrugFlag": {特殊药品标志/0否,1是},
                "drugExpiryDate": {药品有效期},
                "lmtUsedFlag": {限制使用标志/1是,0否},
                "medicareCatalogId": {目录ID},
                "genericDrug": {仿制药一致性评价药品},
                "anotherName": {别名},
                "drugSpecs": {规格},
                "drugCategoryName": {药品类别名称},
                "nationDosageForm": {国家医保药品目录剂型},
                "uniqueNum": {唯一记录号},
                "talkDrugName": {医保谈判药品名称},
                "packMaterialName": {包装材质名称},
                "registrationNumStartDate": {药品注册证号开始日期},
                "ver": {版本号},
                "otcFlagName": {非处方药标志名称},
                "dosageForm": {药品剂型},
                "factoryName": {生产企业名称},
                "verName": {版本名称},
                "drugSpecsCode": {药品规格代码},
                "nationMedicalRemark": {国家医保药品目录备注},
                "registrationNumEndDate": {药品注册证号结束日期},
                "commonNameNum": {通用名编号},
                "registeredName": {注册名称},
                "nationCategoryFlag": {国家医保药品目录甲乙类标识},
                "pinyin": {拼音助记码},
                "drugName": {药品商品名},
                "packQty": {包装数量},
                "registeredDosaForm": {注册剂型},
                "startDate": {开始日期},
                "dosage": {每次用量},
                "dosageFormName": {药品剂型名称},
                "englishName": {英文名称},
                "categroyType": {目录类别},
                "updateDate": {更新时间},
                "drugCode": {医疗目录编码},
                "validFlag": {有效标志/0无效,1有效} ,
                "remark": {备注},
                "marketStateName": {市场状态名称},
                "standardCode": {药监本位码},
                "nationDrugCode": {国家药品编号},
                "packSpecs": {包装规格},
                "dataCreated": "2021-10-16 09:54:36",
                "spLmtpricDrugFlag": {特殊限价药品标志/0否,1是},
                "medicareDrugId": 14437,
                "basMednFlag": {基本药物标志/1是,2否},
                "registeredSpecs": {注册规格},
                "registrationNum": {药品注册证号},
                "medicareWestDrugId": 1,
                "marketState": {市场状态},
                "packMaterial": {包装材质},
                "otcFlag": {非处方药标志},
                "healthCommissionDrugCode": {卫健委药品编码},
                "tcmpatFlag": {中成药标志/1	是	2	否},
                "hiNegoDrugFlag": {医保谈判药品标志},
                "registeredDrugSpecsCode": {注册规格代码}
            }
        ],
        "pageable": {
            "sort": {
                "sorted": true,
                "unsorted": false,
                "empty": false
            },
            "offset": 0,
            "pageNumber": 0,
            "pageSize": 20,
            "unpaged": false,
            "paged": true
        },
        "totalElements": 1,
        "totalPages": 1,
        "last": true,
        "number": 0,
        "size": 20,
        "sort": {
            "sorted": true,
            "unsorted": false,
            "empty": false
        },
        "numberOfElements": 1,
        "first": true,
        "empty": false
    }
}
```
##### 25.2 医保中药饮片目录查询
请求地址：/sys/sysMedicareDrug/queryMedicareChn

请求参数：
```json
{
    "drugName":{药品名称},
    "drugCode":{药品编码},
}
```
响应参数：
```json
{
    "code": 0,
    "message": "操作成功",
    "data": {
        "content": [
            {
                "safeMeasure": {安全计量},
                "categroyType": {目录类别},
                "updateDate": {更新时间},
                "drugCode": {医疗目录编码},
                "endDate": {结束日期},
                "validFlag": {有效标志/0无效,1有效},
                "processMethod": {炮制方法},
                "provincePayPolicy": {省级医保支付政策},
                "effectClass": {功效分类},
                "lastUpdated": null,
                "medicareCatalogId": {目录ID},
                "variety": {品种},
                "qualityLeve": {质量等级},
                "dataCreated": "2021-10-16 09:54:00",
                "uniqueNum": {唯一记录号},
                "dataUpdateTime": {数据更新时间},
                "materialSource": {药材种来源},
                "medicareDrugId": {医保目录ID},
                "medicinalTaste": {性味},
                "standardName": {标准名称},
                "ver": {版本号},
                "medicareChnDrugId": 1,
                "standardPage": {标准页码},
                "dataCreatedTime": {数据创建时间},
                "generalUsage": {常规用法},
                "verName": {版本名称},
                "nationPayPolicy": {国家医保支付政策},
                "materialName": {药材名称},
                "attribution": {归经},
                "drugName": {单味药名称},
                "singleFlag": {单复方标志},
                "indication": {功能主治},
                "medicinalPart": {药用部位},
                "standardElecFile": {标准电子档案},
                "startDate": {开始日期},
                "chnYear": {中草药年份}
            }
        ],
        "pageable": {
            "sort": {
                "sorted": true,
                "unsorted": false,
                "empty": false
            },
            "offset": 0,
            "pageSize": 20,
            "pageNumber": 0,
            "unpaged": false,
            "paged": true
        },
        "totalElements": 1,
        "totalPages": 1,
        "last": true,
        "number": 0,
        "size": 20,
        "sort": {
            "sorted": true,
            "unsorted": false,
            "empty": false
        },
        "numberOfElements": 1,
        "first": true,
        "empty": false
    }
}
```
##### 26.3 医保医用耗材目录查询
请求地址：/sys/sysMedicareDrug/queryConsumable

请求参数：
```json
{
    "drugName":{药品名称},
    "drugCode":{药品编码},
}
```
响应参数：
```json
{
    "code": 0,
    "message": "操作成功",
    "data": {
        "content": [
            {
                "factoryCode": {生产企业编号},
                "deviceUniqueNum": {医疗器械唯一标识码},
                "proNum": {产品型号},
                "caseFlag": {机套标志/1是,2否},
                "endDate": {结束日期},
                "packUnit": {包装单位},
                "registeredPsnName": {注册备案人名称},
                "genericName": {医保通用名},
                "picNum": {产品图片编号},
                "Instruction": {产品使用方法},
                "proStandard": {产品标准},
                "specsModel": {规格型号},
                "qualityStandard": {产品质量标准},
                "lastUpdated": null,
                "lmtUsedFlag": {限制使用标志/1是,0否},
                "productImage": {产品影像},
                "medicareCatalogId": {目录ID},
                "consumClass": {耗材分类},
                "minsalunt": {最小销售单位},
                "uniqueNum": {唯一记录号},
                "specialMachinename": {专机名称},
                "strucPart": {结构及组成},
                "ver": 版本号,
                "factoryName": {生产企业名称},
                "verName": {版本名称},
                "perfStrucPart": {性能结构与组成},
                "dspoUsedFlag": {一次性使用标志/1是,2否},
                "imptMatlHmorgnFlag": {植入材料和人体器官标志/1是,2否},
                "genericNameCode": {医保通用名代码},
                "approvedDate": {批准日期},
                "deviceCategory": {医疗器械管理类别},
                "smallestUseUnit": {最小使用单位},
                "proExpiryDate": {产品有效期},
                "imptItvtClssFlag": {植入或介入类标志/0	否,1植入,2介入},
                "registeredPsnAds": {注册备案人住所},
                "drugName": {耗材名称},
                "restrictedUserFlag": {医保限用范围},
                "packQty": {包装数量},
                "specialFlag": {专机专用标志},
                "sonsumSpecs": {规格},
                "startDate": {开始日期},
                "setName": 组套名称,
                "categroyType": {目录类别},
                "updateDate": {更新时间},
                "drugCode": {医疗目录编码},
                "elecFile": {注册或备案证电子档案},
                "validFlag": {有效标志/0无效,1有效},
                "agentEnterprise": {代理人企业},
                "registrationName": {注册备案产品名称},
                "dspoUsedName": {一次性使用标志名称},
                "prodAreaTypeName": {生产地类别名称},
                "consumSpecsCode": {规格代码},
                "highvalMcsFlag": {高值耗材标志/1是,2否},
                "manual": {说明书},
                "otherSupportMaterial": {其他证明材料},
                "medicalMaterialClassCode": {医用材料分类代码},
                "sterilizatioName": {灭菌标志名称},
                "imptItvtClssName": {植入或介入类名称},
                "consumMaterial": {耗材材质},
                "packSpecs": {包装规格},
                "scope": {适用范围},
                "dataCreated": "2021-10-16 09:49:07",
                "proAds": {生产地址},
                "certEndTime": {注册证有效期结束时间},
                "medicareDrugId": {医保目录ID},
                "conversionRatio": {产品转换比},
                "medicareConsumableId": {医用耗材ID},
                "certStartTime": {注册证有效期开始时间},
                "proCountryOrRegion": {生产国或地区},
                "registrationNum": {注册备案号},
                "sterilizationFlag": {灭菌标志},
                "afterSalesAgency": 售后服务机构,
                "materialCode": {材质代码},
                "agentEnterpriseAds": {代理人企业地址},
                "packMaterial": {产品包装材质},
                "prodAreaType": {生产地类别},
                "otherContent": {其他内容},
                "deviceCategoryName": {医疗器械管理类别名称}
            }
        ],
        "pageable": {
            "sort": {
                "sorted": true,
                "unsorted": false,
                "empty": false
            },
            "offset": 0,
            "pageNumber": 0,
            "pageSize": 20,
            "unpaged": false,
            "paged": true
        },
        "totalElements": 2,
        "totalPages": 1,
        "last": true,
        "number": 0,
        "size": 20,
        "sort": {
            "sorted": true,
            "unsorted": false,
            "empty": false
        },
        "numberOfElements": 2,
        "first": true,
        "empty": false
    }
}
```

##### 26.4 医保西药中成药目录更新
请求地址：/sys/sysMedicareDrug/updateMedicareWestCatalog

请求参数：
```json
{
    "ver":{更新版本号}
}
```
响应参数：
```json
{
    {标准响应}
}
```


##### 26.5 医保中药饮片目录更新
请求地址：/sys/sysMedicareDrug/updateMedicareChnCatalog

请求参数：
```json
{
    "ver":{更新版本号}
}
```
响应参数：
```json
{
    {标准响应}
}
```


##### 26.6 医保医用耗材目录更新
请求地址：/sys/sysMedicareDrug/updateMedicareConsumCatalog

请求参数：
```json
{
    "ver":{更新版本号}
}
```
响应参数：
```json
{
    {标准响应}
}
```

##### 26.7 批量对照用户医保商品
请求地址：/sys/sysMedicareGoodsRel/batchContrastGoods

请求参数：
```json
{
    "apiUserIdList":{apiUserId数组}
}
```
响应参数：
```json
{
    {标准响应}
}
```



#### 27 对照结果查询接口
##### 27.1 西药对照结果查询接口
请求地址：/web/medicareDrugsGoodsRel/queryMedicareWest

请求参数：
```json
```
响应参数：
```json
{
    "code": 0,
    "message": "操作成功",
    "data": {
        "content": [
            {
                "chemicalName": null,
                "endDate": {结束日期},
                "limitUseScope": null,
                "companyName": null,
                "remark": null,
                "lastUpdated": null,
                "spDrugFlag": null,
                "approved": null,
                "drugExpiryDate": null,
                "lmtUsedFlag": null,
                "price": null,
                "supplementElecFile": null,
                "anotherName": "啥东西",
                "uniqueNum": null,
                "minsalunt": null,
                "dataUpdateTime": null,
                "packMaterialName": null,
                "dosageForm": null,
                "smallestMeasureUnit": null,
                "factoryName": {生产企业名称},
                "verName": null,
                "drugSpecsCode": null,
                "nationMedicalRemark": null,
                "commonNameNum": null,
                "registeredName": null,
                "miniPreparationUnit": null,
                "packQty": null,
                "indication": null,
                "startDate": {开始日期},
                "smallestUnit": null,
                "dosage": null,
                "drugCode": {医疗目录编码},
                "elecFile": null,
                "validFlag": {有效标志/0无效,1有效},
                "goodsId": "10000",
                "marketStateName": null,
                "manual": null,
                "standardCode": null,
                "unitType": null,
                "nationDrugCode": {国家药品编号},
                "spLmtpricDrugFlag": null,
                "medicareDrugId": 14437,
                "basMednFlag": null,
                "systemId": null,
                "registrationNum": {药品注册证号},
                "dataCreatedTime": null,
                "medicareWestDrugId": 1,
                "packMaterial": null,
                "apiUserId": 5,
                "prodAreaType": null,
                "tcmpatFlag": {中成药标志},
                "instruction": null,
                "Frequency": null,
                "smallestPackUnit": null,
                "registeredDrugSpecsCode": null,
                "listedDrug": null,
                "factoryCode": null,
                "commonName": null,
                "basMednName": null,
                "distributionCompany": null,
                "drugCategory": null,
                "dosingType": null,
                "fiveCode": null,
                "miniPreparationUnitName": null,
                "genericDrug": null,
                "drugSpecs": null,
                "drugCategoryName": null,
                "nationDosageForm": null,
                "talkDrugName": null,
                "registrationNumStartDate": null,
                "ver": null,
                "otcFlagName": null,
                "registrationNumEndDate": null,
                "companyId": null,
                "pinyin": null,
                "smallestUseUnit": null,
                "nationCategoryFlag": {国家医保药品目录甲乙类标识},
                "drugName": {药品商品名},
                "registeredDosaForm": null,
                "valueAddedTaxFlag": null,
                "dosageFormName": null,
                "englishName": null,
                "prodAreaTypeName": null,
                "acidRadical": null,
                "packSpecs": null,
                "subPackFactory": null,
                "dataCreated": "2021-10-18 09:24:40",
                "valueAddedTaxName": null,
                "conversionRatio": null,
                "registeredSpecs": null,
                "miniPackQty": null,
                "marketState": null,
                "otcFlag": null,
                "healthCommissionDrugCode": null,
                "hiNegoDrugFlag": null,
                "smallestPackUnitName": null,
                "childMedication": null,
                "approvalNo": {批准文号},
            }
        ],
        "pageable": {
            "sort": {
                "sorted": true,
                "unsorted": false,
                "empty": false
            },
            "offset": 0,
            "pageNumber": 0,
            "pageSize": 20,
            "unpaged": false,
            "paged": true
        },
        "totalPages": 1,
        "last": true,
        "totalElements": 1,
        "number": 0,
        "size": 20,
        "sort": {
            "sorted": true,
            "unsorted": false,
            "empty": false
        },
        "numberOfElements": 1,
        "first": true,
        "empty": false
    }
}
```

##### 27.2 中药饮片对照结果查询接口
请求地址：/web/medicareDrugsGoodsRel/queryMedicareChn

请求参数：
```json
```
响应参数：
```json
{
    "code": 0,
    "message": "操作成功",
    "data": {
        "content": [
            {
                "safeMeasure": null,
                "drugCode": {医疗目录编码},
                "endDate": {结束日期},
                "validFlag": {有效标志/0无效,1有效},
                "goodsId": {本地标识},
                "processMethod": null,
                "provincePayPolicy": null,
                "lastUpdated": null,
                "effectClass": {功效分类},
                "approved": null,
                "variety": null,
                "price": null,
                "qualityLeve": {质量等级},
                "dataCreated": "2021-10-18 09:25:17",
                "uniqueNum": null,
                "dataUpdateTime": null,
                "materialSource": null,
                "medicareDrugId": 14438,
                "medicinalTaste": null,
                "systemId": null,
                "standardName": null,
                "ver": null,
                "medicareChnDrugId": 1,
                "standardPage": null,
                "dataCreatedTime": null,
                "generalUsage": null,
                "verName": null,
                "nationPayPolicy": null,
                "apiUserId": 5,
                "materialName": null,
                "companyId": null,
                "attribution": null,
                "drugName": {单味药名称},
                "singleFlag": {单复方标志},
                "indication": null,
                "medicinalPart": null,
                "standardElecFile": null,
                "startDate": {开始日期},
                "chnYear": {中草药年份}
            }
        ],
        "pageable": {
            "sort": {
                "sorted": true,
                "unsorted": false,
                "empty": false
            },
            "offset": 0,
            "pageSize": 20,
            "pageNumber": 0,
            "paged": true,
            "unpaged": false
        },
        "totalElements": 1,
        "totalPages": 1,
        "last": true,
        "number": 0,
        "size": 20,
        "sort": {
            "sorted": true,
            "unsorted": false,
            "empty": false
        },
        "numberOfElements": 1,
        "first": true,
        "empty": false
    }
}
```

##### 27.3 医用耗材对照结果查询接口
请求地址：/web/medicareDrugsGoodsRel/queryConsumable

请求参数：
```json
```
响应参数：
```json
{
    "code": 0,
    "message": "操作成功",
    "data": {
        "content": [
            {
                "factoryCode": null,
                "deviceUniqueNum": null,
                "proNum": null,
                "caseFlag": null,
                "endDate": {结束日期},
                "packUnit": null,
                "registeredPsnName": null,
                "genericName": null,
                "picNum": null,
                "proStandard": null,
                "specsModel": null,
                "qualityStandard": null,
                "lastUpdated": null,
                "approved": null,
                "lmtUsedFlag": null,
                "productImage": null,
                "price": null,
                "consumClass": null,
                "minsalunt": null,
                "uniqueNum": null,
                "specialMachinename": null,
                "strucPart": null,
                "ver": null,
                "factoryName": {生产企业名称},
                "verName": null,
                "perfStrucPart": null,
                "dspoUsedFlag": null,
                "imptMatlHmorgnFlag": null,
                "genericNameCode": {医保通用名代码},
                "approvedDate": null,
                "companyId": null,
                "deviceCategory": null,
                "smallestUseUnit": null,
                "proExpiryDate": null,
                "imptItvtClssFlag": null,
                "registeredPsnAds": null,
                "drugName": {耗材名称},
                "restrictedUserFlag": null,
                "packQty": null,
                "specialFlag": null,
                "sonsumSpecs": {规格},
                "startDate": null,
                "setName": null,
                "drugCode": {医疗目录编码},
                "elecFile": null,
                "validFlag": {有效标志/0无效,1有效},
                "goodsId": {本地标识},
                "agentEnterprise": null,
                "registrationName": null,
                "dspoUsedName": null,
                "prodAreaTypeName": null,
                "consumSpecsCode": {规格代码},
                "highvalMcsFlag": null,
                "manual": null,
                "otherSupportMaterial": null,
                "medicalMaterialClassCode": null,
                "sterilizatioName": null,
                "imptItvtClssName": null,
                "consumMaterial": null,
                "packSpecs": null,
                "scope": null,
                "dataCreated": "2021-10-18 09:25:21",
                "proAds": null,
                "certEndTime": null,
                "medicareDrugId": 14439,
                "conversionRatio": null,
                "medicareConsumableId": 1,
                "systemId": null,
                "certStartTime": null,
                "proCountryOrRegion": null,
                "registrationNum": null,
                "sterilizationFlag": null,
                "afterSalesAgency": null,
                "materialCode": null,
                "agentEnterpriseAds": null,
                "packMaterial": null,
                "apiUserId": 5,
                "prodAreaType": null,
                "instruction": null,
                "otherContent": null,
                "deviceCategoryName": null
            }
        ],
        "pageable": {
            "sort": {
                "sorted": true,
                "unsorted": false,
                "empty": false
            },
            "offset": 0,
            "pageNumber": 0,
            "pageSize": 20,
            "unpaged": false,
            "paged": true
        },
        "totalPages": 1,
        "last": true,
        "totalElements": 1,
        "number": 0,
        "size": 20,
        "sort": {
            "sorted": true,
            "unsorted": false,
            "empty": false
        },
        "numberOfElements": 1,
        "first": true,
        "empty": false
    }
}
```

##### 27.5 医保商品对照撤销
请求地址：/web/medicareDrugsGoodsRel/revokeMedicare

请求参数：
```json
{
    "goodsId":{本地标识},
    "drugCode":{医保目录编码}
}
```
响应参数：
```json
{
    {标准响应}
}
```